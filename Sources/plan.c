/*===============================================================================================
/ 	EQUIPAMENTO: Placa LPCPU02
/	  EMPRESA: LUPETEC
/   Rotina de programação de horas e minutos
===============================================================================================*/

#include "plan.h"
#include "systemkernel.h"

void plan_hourUp(void)
{
    plan_ProgUpdate = 2;

    if (plan_hourProgTemp == 0xFF)
    {
        plan_hourProgTemp = 0;
    }
    else if (plan_hourProgTemp < 23)
    {
        plan_hourProgTemp++;
    }
    else
    {
        if (plan_nbL == 0x04)
        {
            plan_hourProgTemp = 0xFF;
        }
        else
        {
            plan_hourProgTemp = 0;
        }
    }
}

void plan_hourDown(void)
{
    plan_ProgUpdate = 2;

    if (plan_hourProgTemp == 0xFF)
    {
        plan_hourProgTemp = 23;
    }
    else if (plan_hourProgTemp > 0)
    {
        plan_hourProgTemp--;
    }
    else
    {
        if (plan_nbL == 0x04)
        {
            plan_hourProgTemp = 0xFF;
        }
        else
        {
            plan_hourProgTemp = 23;
        }
    }
}

void plan_minUp(void)
{

    plan_ProgUpdate = 2;

    if (plan_minProgTemp < 59)
    {
        plan_minProgTemp++;
    }
    else
    {
        plan_hourUp();
        plan_minProgTemp = 0;
    }
}

void plan_minDown(void)
{

    plan_ProgUpdate = 2;

    if (plan_minProgTemp > 0)
    {
        plan_minProgTemp--;
    }
    else
    {
        plan_hourDown();
        plan_minProgTemp = 59;
    }
}