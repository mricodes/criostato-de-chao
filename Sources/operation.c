/*===============================================================================================
/	    EQUIPAMENTO: Placa LPCPU02
/	    EMPRESA: LUPETEC
/     Funcao de comunicao com operador
===============================================================================================*/

#include "operation.h"
#include "systemkernel.h"

void operation_Loop(void)
{
    //operation_TemperatureUpdate();
}

void operation_trimAdd(void)
{
    if (rco_trimValue[0] < 600)
    {
        if (rco_trimValue[0] >= 100)
        {
            rco_trimValue[0] += 50;
        }
        else if (rco_trimValue[0] >= 20)
        {
            rco_trimValue[0] += 5;
        }
        else if (rco_trimValue[0] >= 10)
        {
            rco_trimValue[0] += 2;
        }
        else
        {
            rco_trimValue[0] += 1;
        }
        flash_updateStatus();
    }
}

void operation_trimRemove(void)
{
    if (rco_trimValue[0] > 0)
    {
        if (rco_trimValue[0] >= 100)
        {
            rco_trimValue[0] -= 50;
        }
        else if (rco_trimValue[0] >= 20)
        {
            rco_trimValue[0] -= 5;
        }
        else if (rco_trimValue[0] >= 10)
        {
            rco_trimValue[0] -= 2;
        }
        else
        {
            rco_trimValue[0] -= 1;
        }
        flash_updateStatus();
    }
}

void operation_sectionAdd(void)
{
    if (rco_sectionValue[0] < 6000)
    {
        if (rco_sectionValue[0] >= 1000)
        {
            rco_sectionValue[0] += 500;
        }
        else if (rco_sectionValue[0] >= 200)
        {
            rco_sectionValue[0] += 50;
        }
        else if (rco_sectionValue[0] >= 100)
        {
            rco_sectionValue[0] += 20;
        }
        else if (rco_sectionValue[0] >= 20)
        {
            rco_sectionValue[0] += 10;
        }
        else
        {
            rco_sectionValue[0] += 5;
        }
        flash_updateStatus();
    }
}

void operation_sectionRemove(void)
{
    if (rco_sectionValue[0] > 0)
    {
        if (rco_sectionValue[0] > 1000)
        {
            rco_sectionValue[0] -= 500;
        }
        else if (rco_sectionValue[0] > 200)
        {
            rco_sectionValue[0] -= 50;
        }
        else if (rco_sectionValue[0] > 100)
        {
            rco_sectionValue[0] -= 20;
        }
        else if (rco_sectionValue[0] > 20)
        {
            rco_sectionValue[0] -= 10;
        }
        else
        {
            rco_sectionValue[0] -= 5;
        }
        flash_updateStatus();
    }
}

void operation_tempcamaraUp(void)
{
    control_temperatureFlag = 0;
    rco_TimeSetCam = 2;
    if (operation_TSetCamaraN < 35)
    {
        operation_TSetCamaraN = operation_TSetCamaraN + 1;
    }
}

void operation_tempcamaraDown(void)
{
    control_temperatureFlag = 0;
    rco_TimeSetCam = 2;
    if (operation_TSetCamaraN > -50)
    {
        operation_TSetCamaraN = operation_TSetCamaraN - 1;
    }
}

void operation_temppeltierUp(void)
{
    control_temperatureFlag = 0;
    if (operation_TSetPeltierN < 35)
    {
        operation_TSetPeltierN = operation_TSetPeltierN + 1;
    }
}

void operation_temppeltierDown(void)
{
    control_temperatureFlag = 0;
    if (operation_TSetPeltierN > -50)
    {
        operation_TSetPeltierN = operation_TSetPeltierN - 1;
    }
}

void operation_tempsampleUp(void)
{
    if (control_TSetAmostraN < 35)
    {
        control_TSetAmostraN = control_TSetAmostraN + 1;
        control_Temp = control_TSetAmostraN >> 16;
        rco_fTsetAmostra[0] = control_Temp;
        control_Temp = control_TSetAmostraN >> 8;
        rco_fTsetAmostra[1] = control_Temp;
        rco_fTsetAmostra[2] = control_TSetAmostraN;
    }
}

void operation_tempsampleDown(void)
{
    if (control_TSetAmostraN > -50)
    {
        control_TSetAmostraN = control_TSetAmostraN - 1;
        control_Temp = control_TSetAmostraN >> 16;
        rco_fTsetAmostra[0] = control_Temp;
        control_Temp = control_TSetAmostraN >> 8;
        rco_fTsetAmostra[1] = control_Temp;
        rco_fTsetAmostra[2] = control_TSetAmostraN;
    }
}

void operation_retraUp(void)
{
    if (control_RetracValue < 100)
    {
        control_RetracValue++;
        flash_updateStatus();
    }
}

void operation_retraDown(void)
{
    if (control_RetracValue > 0)
    {
        control_RetracValue--;
        flash_updateStatus();
    }
}

void operation_standardH(unsigned char Channel)
{
    adc_Taux1 = adc_Value[Channel];
    adc_T1 = adc_Taux1 * 1000;
}

void operation_Coefficient(unsigned char Channel, unsigned char Band)
{

    //Equation used: y = ax+b
    adc_Taux1 = adc_Value[Channel];
    adc_T2 = adc_Taux1 * 1000;

    //(y1-y2) = (ax1 - ax2)
    control_Temp = (adc_T1 - adc_T2);

    //Constante 1/a
    A_CH[Channel] = control_Temp / 47;
    B_CH[Channel] = (adc_T2 / A_CH[Channel]);

    control_Temp = A_CH[Channel] >> 16;

    A_1[Channel] = control_Temp;
    control_Temp = A_CH[Channel] >> 8;

    A_2[Channel] = control_Temp;
    A_3[Channel] = A_CH[Channel];

    control_Temp = B_CH[Channel] >> 16;
    B_1[Channel] = control_Temp;

    control_Temp = B_CH[Channel] >> 8;
    B_2[Channel] = control_Temp;

    B_3[Channel] = B_CH[Channel];
    operation_chCalibrado[Channel] = 1;
    flash_updateStatus();
}

void operation_TemperatureUpdate(void)
{
    if (rco_ScreenState != WAIT_SCREEN)
    {
        control_TemperatureRead(TEMPERATURE_AMBIENT);
        control_TAmbiente = adc_TOut;

        control_TemperatureRead(TEMPERATURE_CHAMBER);
        control_TCamara = adc_TOut;

        control_TemperatureRead(TEMPERATURE_PELTIER);
        control_TPeltier = adc_TOut;

        control_TemperatureRead(TEMPERATURE_SAMPLE);
        control_TAmostra = adc_TOut;
    }
}

void operation_chamberStandUp(void)
{
    control_AutotempFlag = 0;
    if (operation_TstandCamaraN < 40)
    {
        operation_TstandCamaraN = operation_TstandCamaraN + 1;
    }
}

void operation_chamberStandDown(void)
{
    control_AutotempFlag = 0;
    if (operation_TstandCamaraN > -110)
    {
        operation_TstandCamaraN = operation_TstandCamaraN - 1;
    }
}

void operation_chamberDefrostUp(void)
{
    control_AutotempFlag = 0;
    if (operation_TdefrostCamaraN < 40)
    {
        operation_TdefrostCamaraN = operation_TdefrostCamaraN + 1;
    }
}

void operation_chamberDefrostDown(void)
{
    control_AutotempFlag = 0;
    if (operation_TdefrostCamaraN > -110)
    {
        operation_TdefrostCamaraN = operation_TdefrostCamaraN - 1;
    }
}

void operation_peltierStandUp(void)
{
    control_AutotempFlag = 0;
    if (operation_TstandPeltierN < 40)
    {
        operation_TstandPeltierN = operation_TstandPeltierN + 1;
    }
}

void operation_peltierStandDown(void)
{
    control_AutotempFlag = 0;
    if (operation_TstandPeltierN > -110)
    {
        operation_TstandPeltierN = operation_TstandPeltierN - 1;
    }
}

void operation_peltierDefrostUp(void)
{
    control_AutotempFlag = 0;
    if (operation_TdefrostPeltierN < 40)
    {
        operation_TdefrostPeltierN = operation_TdefrostPeltierN + 1;
    }
}

void operation_peltierDefrostDown(void)
{
    control_AutotempFlag = 0;
    if (operation_TdefrostPeltierN > -110)
    {
        operation_TdefrostPeltierN = operation_TdefrostPeltierN - 1;
    }
}
