/*===============================================================================================
/	  EQUIPAMENTO: Placa LPCPU02
/	  EMPRESA: LUPETEC
/   Rotinas de conversao analogico-digital
===============================================================================================*/


#include "adc.h"

void adc_Init(void){

    ADC1SC1 = 0b01000011;

    ADC1SC2 = 0b00000000;

    ADC1CFG = 0b01101001;

    APCTL1 = 0b11111000;
    
}

