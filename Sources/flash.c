/*===============================================================================================
/ 	EQUIPAMENTO: Placa LPCPU02
/	  EMPRESA: LUPETEC
/   Rotina de manipulacao de memoria flash
===============================================================================================*/

//Includes
#include "flash.h"
#include "systemkernel.h"
#include "plan.h"

byte res;

/////////////////////////////////////////////////////////////////////////////////////////////
// Lê um endereço de memória FLASH
/////////////////////////////////////////////////////////////////////////////////////////////
unsigned char ReadFlash(unsigned int endereco)
{
  unsigned char dado;
  asm {
        ldhx  endereco
        lda   0,X
    		sta	  dado
  }
  return dado;
  /*
  unsigned char *ponteiro;    
  ponteiro = (char*) endereco; //guarda o endereço da memória em um ponteiro
  return (*ponteiro); //retorna o valor armazenado no endereço indicado pelo ponteiro
  */
}
/////////////////////////////////////////////////////////////////////////////////////////////
//Escreve em um endereço da memória FLASH
/////////////////////////////////////////////////////////////////////////////////////////////
unsigned char WriteFlash(unsigned int endereco, unsigned char dado, unsigned char flagPrimeiroByte)
{
  unsigned char *ponteiro;
  ponteiro = (char *)endereco; //guarda o endereço da memória em um ponteiro
  if (flagPrimeiroByte)
  {

    FlashErase(ponteiro);
    if (FSTAT_FACCERR)
    {
      FSTAT_FACCERR = 1;
    } //Verifica e apaga flag de erro de acesso a FLASH
  }
  FlashProg(ponteiro, dado); //Chama a função de programação da FLASH
  if (FSTAT_FACCERR || FSTAT_FPVIOL)
  {
    return (1);
  } //se houver erro, retorna 1. Caso contrário, retorna 0
  else
  {
    return (0);
  }
}
/////////////////////////////////////////////////////////////////////////////////////////////
// Apaga uma página na FLASH
/////////////////////////////////////////////////////////////////////////////////////////////
unsigned char PageEraseFlash(unsigned int endereco)
{
  unsigned char *ponteiro;
  ponteiro = (char *)endereco; //guarda o endereço da memória em um ponteiro
  if (FSTAT_FACCERR)
  {
    FSTAT_FACCERR = 1;
  }                     //Verifica e apaga flag de erro de acesso a FLASH
  FlashErase(ponteiro); //Chama a função de programação da FLASH
  if (FSTAT_FACCERR || FSTAT_FPVIOL)
  {
    return (1);
  } //se houver erro, retorna 1. Caso contrário, retorna 0
  else
  {
    return (0);
  }
}

/////////////////////////////////////////////////////////////////////////////////////////////
// Rotina de inicializacao
// erro = 0 sem erro, erro > 0 ocorreu erro na gravacao de algum dado
/////////////////////////////////////////////////////////////////////////////////////////////
unsigned char flash_Update(void)
{

  unsigned char erro = 0;
  unsigned int flash_checkSum = 0;

  unsigned char auxH = 0;
  unsigned char auxM = 0;
  unsigned char i = 0;
  flash_chcheckSum = 0;

  //unsigned char auxL = 0;
  if (flash_delUpdate)
  {
    flash_delUpdate = 0;
    flash_flagUpdate = 0;

    erro += WriteFlash(0x8000, display_Language, 1);
    erro += WriteFlash(0x8001, flash_userPassword[0], 0);
    erro += WriteFlash(0x8002, flash_userPassword[1], 0);
    erro += WriteFlash(0x8003, flash_userPassword[2], 0);
    erro += WriteFlash(0x8004, flash_userPassword[3], 0);
    erro += WriteFlash(0x8005, rco_numpulseAdjustment, 0);
    erro += WriteFlash(0x8006, rco_periodAdjustment, 0);

    auxH = rco_trimValue[0] / 256;
    erro += WriteFlash(0x8007, auxH, 0);
    erro += WriteFlash(0x8008, rco_trimValue[0], 0);

    auxH = rco_sectionValue[0] / 256;
    erro += WriteFlash(0x8009, auxH, 0);
    erro += WriteFlash(0x800A, rco_sectionValue[0], 0);

    auxH = rco_trimValue[1] / 256;
    erro += WriteFlash(0x800B, auxH, 0);
    erro += WriteFlash(0x800C, rco_trimValue[1], 0);

    auxH = rco_sectionValue[1] / 256;
    erro += WriteFlash(0x800D, auxH, 0);
    erro += WriteFlash(0x800E, rco_sectionValue[1], 0);

    auxH = rco_trimValue[2] / 256;
    erro += WriteFlash(0x800F, auxH, 0);
    erro += WriteFlash(0x8010, rco_trimValue[2], 0);

    auxH = rco_sectionValue[2] / 256;
    erro += WriteFlash(0x8011, auxH, 0);
    erro += WriteFlash(0x8012, rco_sectionValue[2], 0);

    auxH = rco_trimValue[3] / 256;
    erro += WriteFlash(0x8013, auxH, 0);
    erro += WriteFlash(0x8014, rco_trimValue[3], 0);

    auxH = rco_sectionValue[3] / 256;
    erro += WriteFlash(0x8015, auxH, 0);
    erro += WriteFlash(0x8016, rco_sectionValue[3], 0);

    auxH = rco_trimValue[4] / 256;
    erro += WriteFlash(0x8017, auxH, 0);
    erro += WriteFlash(0x8018, rco_trimValue[4], 0);

    auxH = rco_sectionValue[4] / 256;
    erro += WriteFlash(0x8019, auxH, 0);
    erro += WriteFlash(0x801A, rco_sectionValue[4], 0);

    auxH = rco_trimValue[5] / 256;
    erro += WriteFlash(0x801B, auxH, 0);
    erro += WriteFlash(0x801C, rco_trimValue[5], 0);

    auxH = rco_sectionValue[5] / 256;
    erro += WriteFlash(0x801D, auxH, 0);
    erro += WriteFlash(0x801E, rco_sectionValue[5], 0);

    auxH = rco_trimValue[6] / 256;
    erro += WriteFlash(0x801F, auxH, 0);
    erro += WriteFlash(0x8020, rco_trimValue[6], 0);

    auxH = rco_sectionValue[6] / 256;
    erro += WriteFlash(0x8021, auxH, 0);
    erro += WriteFlash(0x8022, rco_sectionValue[6], 0);
    erro += WriteFlash(0x8023, control_RetracValue, 0);

    erro += WriteFlash(0x8024, A_1[0], 0);
    erro += WriteFlash(0x8025, A_2[0], 0);
    erro += WriteFlash(0x8026, A_3[0], 0);
    erro += WriteFlash(0x8027, B_1[0], 0);
    erro += WriteFlash(0x8028, B_2[0], 0);
    erro += WriteFlash(0x8029, B_3[0], 0);

    erro += WriteFlash(0x802A, A_1[1], 0);
    erro += WriteFlash(0x802B, A_2[1], 0);
    erro += WriteFlash(0x802C, A_3[1], 0);
    erro += WriteFlash(0x802D, B_1[1], 0);
    erro += WriteFlash(0x802E, B_2[1], 0);
    erro += WriteFlash(0x802F, B_3[1], 0);

    erro += WriteFlash(0x8030, A_1[2], 0);
    erro += WriteFlash(0x8031, A_2[2], 0);
    erro += WriteFlash(0x8032, A_3[2], 0);
    erro += WriteFlash(0x8033, B_1[2], 0);
    erro += WriteFlash(0x8034, B_2[2], 0);
    erro += WriteFlash(0x8035, B_3[2], 0);

    erro += WriteFlash(0x8036, A_1[3], 0);
    erro += WriteFlash(0x8037, A_2[3], 0);
    erro += WriteFlash(0x8038, A_3[3], 0);
    erro += WriteFlash(0x8039, B_1[3], 0);
    erro += WriteFlash(0x803A, B_2[3], 0);
    erro += WriteFlash(0x803B, B_3[3], 0);

    erro += WriteFlash(0x803C, A_1[4], 0);
    erro += WriteFlash(0x803D, A_2[4], 0);
    erro += WriteFlash(0x803E, A_3[4], 0);
    erro += WriteFlash(0x803F, B_1[4], 0);
    erro += WriteFlash(0x8040, B_2[4], 0);
    erro += WriteFlash(0x8041, B_3[4], 0);

    erro += WriteFlash(0x8042, rco_fTsetCamara[0], 0);
    erro += WriteFlash(0x8043, rco_fTsetCamara[1], 0);
    erro += WriteFlash(0x8044, rco_fTsetCamara[2], 0);

    erro += WriteFlash(0x8045, rco_fTsetAmostra[0], 0);
    erro += WriteFlash(0x8046, rco_fTsetAmostra[1], 0);
    erro += WriteFlash(0x8047, rco_fTsetAmostra[2], 0);

    erro += WriteFlash(0x8048, rco_fTsetPeltier[0], 0);
    erro += WriteFlash(0x8049, rco_fTsetPeltier[1], 0);
    erro += WriteFlash(0x804A, rco_fTsetPeltier[2], 0);

    erro += WriteFlash(0x804B, rco_fTsetExtra[0], 0);
    erro += WriteFlash(0x804C, rco_fTsetExtra[1], 0);
    erro += WriteFlash(0x804D, rco_fTsetExtra[2], 0);

    for (i = 0; i < 35; i++)
    {
      erro += WriteFlash((0x804E + i), plan_hourProg[i], 0);
      erro += WriteFlash((0x8083 + i), plan_minProg[i], 0);
    }

    erro += WriteFlash(0x80A6, control_HistTemp[0], 0);
    erro += WriteFlash(0x80A7, control_HistTemp[1], 0);
    erro += WriteFlash(0x80A8, control_HistTemp[2], 0);
    erro += WriteFlash(0x80A9, control_HistTemp[3], 0);
    erro += WriteFlash(0x80AA, control_HistTemp[4], 0);

    erro += WriteFlash(0x80AB, control_autoDefrostCamera[0], 0);
    erro += WriteFlash(0x80AC, control_autoDefrostCamera[1], 0);
    erro += WriteFlash(0x80AD, control_autoDefrostCamera[2], 0);

    erro += WriteFlash(0x80AE, control_autoDefrostPeltier[0], 0);
    erro += WriteFlash(0x80AF, control_autoDefrostPeltier[1], 0);
    erro += WriteFlash(0x80B0, control_autoDefrostPeltier[2], 0);

    erro += WriteFlash(0x80B1, control_autoStandCamera[0], 0);
    erro += WriteFlash(0x80B2, control_autoStandCamera[1], 0);
    erro += WriteFlash(0x80B3, control_autoStandCamera[2], 0);

    erro += WriteFlash(0x80B4, control_autoStandPeltier[0], 0);
    erro += WriteFlash(0x80B5, control_autoStandPeltier[1], 0);
    erro += WriteFlash(0x80B6, control_autoStandPeltier[2], 0);

    erro += WriteFlash(0x80B7, adc_Offset[0], 0);
    erro += WriteFlash(0x80B8, adc_Offset[1], 0);
    erro += WriteFlash(0x80B9, adc_Offset[2], 0);
    erro += WriteFlash(0x80BA, adc_Offset[3], 0);
    erro += WriteFlash(0x80BB, adc_Offset[4], 0);

    erro += WriteFlash(0x80BC, operation_chCalibrado[0], 0);
    erro += WriteFlash(0x80BD, operation_chCalibrado[1], 0);
    erro += WriteFlash(0x80BE, operation_chCalibrado[2], 0);
    erro += WriteFlash(0x80BF, operation_chCalibrado[3], 0);
    erro += WriteFlash(0x80C0, operation_chCalibrado[4], 0);

    flash_checkSum = 0;
    flash_checkSum += display_Language;
    flash_checkSum += flash_userPassword[0];
    flash_checkSum += flash_userPassword[1];
    flash_checkSum += flash_userPassword[2];
    flash_checkSum += flash_userPassword[3];
    flash_checkSum += rco_numpulseAdjustment;
    flash_checkSum += rco_periodAdjustment;

    flash_checkSum += rco_trimValue[0];
    flash_checkSum += rco_sectionValue[0];
    flash_checkSum += rco_trimValue[1];
    flash_checkSum += rco_sectionValue[1];
    flash_checkSum += rco_trimValue[2];
    flash_checkSum += rco_sectionValue[2];
    flash_checkSum += rco_trimValue[3];
    flash_checkSum += rco_sectionValue[3];
    flash_checkSum += rco_trimValue[4];
    flash_checkSum += rco_sectionValue[4];
    flash_checkSum += rco_trimValue[5];
    flash_checkSum += rco_sectionValue[5];
    flash_checkSum += rco_trimValue[6];
    flash_checkSum += rco_sectionValue[6];
    flash_checkSum += control_RetracValue;
    
    //testar o initial Sum

  flash_checkSum += adc_Offset[0];
  flash_checkSum += adc_Offset[1];
  flash_checkSum += adc_Offset[2];
  flash_checkSum += adc_Offset[3];
  flash_checkSum += adc_Offset[4];

  flash_checkSum += A_1[0];
  flash_checkSum += A_2[0];
  flash_checkSum += A_3[0];
  flash_checkSum += B_1[0];
  flash_checkSum += B_2[0];
  flash_checkSum += B_3[0];

  flash_checkSum += A_1[1];
  flash_checkSum += A_2[1];
  flash_checkSum += A_3[1];
  flash_checkSum += B_1[1];
  flash_checkSum += B_2[1];
  flash_checkSum += B_3[1];

  flash_checkSum += A_1[2];
  flash_checkSum += A_2[2];
  flash_checkSum += A_3[2];
  flash_checkSum += B_1[2];
  flash_checkSum += B_2[2];
  flash_checkSum += B_3[2];

  flash_checkSum += A_1[3];
  flash_checkSum += A_2[3];
  flash_checkSum += A_3[3];
  flash_checkSum += B_1[3];
  flash_checkSum += B_2[3];
  flash_checkSum += B_3[3];

  flash_checkSum += A_1[4];
  flash_checkSum += A_2[4];
  flash_checkSum += A_3[4];
  flash_checkSum += B_1[4];
  flash_checkSum += B_2[4];
  flash_checkSum += B_3[4];

  flash_checkSum += rco_fTsetCamara[0];
  flash_checkSum += rco_fTsetCamara[1];
  flash_checkSum += rco_fTsetCamara[2];

  flash_checkSum += rco_fTsetAmostra[0];
  flash_checkSum += rco_fTsetAmostra[1];
  flash_checkSum += rco_fTsetAmostra[2];

  flash_checkSum += rco_fTsetPeltier[0];
  flash_checkSum += rco_fTsetPeltier[1];
  flash_checkSum += rco_fTsetPeltier[2];

  flash_checkSum += rco_fTsetExtra[0];
  flash_checkSum += rco_fTsetExtra[1];
  flash_checkSum += rco_fTsetExtra[2];

  for (i = 0; i < 35; i++)
  {
    flash_checkSum += plan_hourProg[i];
    flash_checkSum += plan_minProg[i];
  }

  flash_checkSum += control_HistTemp[0];
  flash_checkSum += control_HistTemp[1];
  flash_checkSum += control_HistTemp[2];
  flash_checkSum += control_HistTemp[3];
  flash_checkSum += control_HistTemp[4];

  flash_checkSum += control_autoDefrostCamera[0];
  flash_checkSum += control_autoDefrostCamera[1];
  flash_checkSum += control_autoDefrostCamera[2];

  flash_checkSum += control_autoDefrostPeltier[0];
  flash_checkSum += control_autoDefrostPeltier[1];
  flash_checkSum += control_autoDefrostPeltier[2];

  flash_checkSum += control_autoStandCamera[0];
  flash_checkSum += control_autoStandCamera[1];
  flash_checkSum += control_autoStandCamera[2];

  flash_checkSum += control_autoStandPeltier[0];
  flash_checkSum += control_autoStandPeltier[1];
  flash_checkSum += control_autoStandPeltier[2];

  flash_checkSum += operation_chCalibrado[0];
  flash_checkSum += operation_chCalibrado[1];
  flash_checkSum += operation_chCalibrado[2];
  flash_checkSum += operation_chCalibrado[3];
  flash_checkSum += operation_chCalibrado[4];

  while(flash_checkSum > 255){
    flash_checkSum = flash_checkSum - 255;
    flash_chcheckSum++;
  }

    erro += WriteFlash(0x80FF, flash_chcheckSum, 0);
  }
  return (erro);
}
/////////////////////////////////////////////////////////////////////////////////////////////
// Rotina de inicializacao
// erro = 0 sem erro, erro > 0 ocorreu erro na gravacao de algum dado
/////////////////////////////////////////////////////////////////////////////////////////////
unsigned char flash_updateStatus(void)
{
  flash_delUpdate = 1;
}
/////////////////////////////////////////////////////////////////////////////////////////////
// Rotina de inicializacao
/////////////////////////////////////////////////////////////////////////////////////////////
void flash_Init(void)
{
  unsigned char i, j;
  if (FSTAT_FACCERR)
  {
    FSTAT_FACCERR = 1;
  }
  FCDIV = vFCDIV;

  flash_initialSum = 0;

  display_Language = ReadFlash(0x8000);
  flash_userPassword[0] = ReadFlash(0x8001);
  flash_userPassword[1] = ReadFlash(0x8002);
  flash_userPassword[2] = ReadFlash(0x8003);
  flash_userPassword[3] = ReadFlash(0x8004);
  rco_numpulseAdjustment = ReadFlash(0x8005);
  rco_periodAdjustment = ReadFlash(0x8006);

  rco_trimValue[0] = ((ReadFlash(0x8007) << 8) & 0x00FF00) | ReadFlash(0x8008);
  rco_sectionValue[0] = ((ReadFlash(0x8009) << 8) & 0x00FF00) | ReadFlash(0x800A);
  rco_trimValue[1] = ((ReadFlash(0x800B) << 8) & 0x00FF00) | ReadFlash(0x800C);
  rco_sectionValue[1] = ((ReadFlash(0x800D) << 8) & 0x00FF00) | ReadFlash(0x800E);
  rco_trimValue[2] = ((ReadFlash(0x800F) << 8) & 0x00FF00) | ReadFlash(0x8010);
  rco_sectionValue[2] = ((ReadFlash(0x8011) << 8) & 0x00FF00) | ReadFlash(0x8012);
  rco_trimValue[3] = ((ReadFlash(0x8013) << 8) & 0x00FF00) | ReadFlash(0x8014);
  rco_sectionValue[3] = ((ReadFlash(0x8015) << 8) & 0x00FF00) | ReadFlash(0x8016);
  rco_trimValue[4] = ((ReadFlash(0x8017) << 8) & 0x00FF00) | ReadFlash(0x8018);
  rco_sectionValue[4] = ((ReadFlash(0x8019) << 8) & 0x00FF00) | ReadFlash(0x801A);
  rco_trimValue[5] = ((ReadFlash(0x801B) << 8) & 0x00FF00) | ReadFlash(0x801C);
  rco_sectionValue[5] = ((ReadFlash(0x801D) << 8) & 0x00FF00) | ReadFlash(0x801E);
  rco_trimValue[6] = ((ReadFlash(0x801F) << 8) & 0x00FF00) | ReadFlash(0x8020);
  rco_sectionValue[6] = ((ReadFlash(0x8021) << 8) & 0x00FF00) | ReadFlash(0x8022);
  control_RetracValue = ReadFlash(0x8023);

  A_1[0] = ReadFlash(0x8024);
  A_2[0] = ReadFlash(0x8025);
  A_3[0] = ReadFlash(0x8026);
  control_Temp = A_1[0];
  A_CH[0] = control_Temp << 16;
  control_Temp = A_2[0];
  A_CH[0] |= control_Temp << 8;
  A_CH[0] |= A_3[0];

  B_1[0] = ReadFlash(0x8027);
  B_2[0] = ReadFlash(0x8028);
  B_3[0] = ReadFlash(0x8029);
  control_Temp = B_1[0];
  B_CH[0] = control_Temp << 16;
  control_Temp = B_2[0];
  B_CH[0] |= control_Temp << 8;
  B_CH[0] |= B_3[0];

  A_1[1] = ReadFlash(0x802A);
  A_2[1] = ReadFlash(0x802B);
  A_3[1] = ReadFlash(0x802C);
  control_Temp = A_1[1];
  A_CH[1] = control_Temp << 16;
  control_Temp = A_2[1];
  A_CH[1] |= control_Temp << 8;
  A_CH[1] |= A_3[1];

  B_1[1] = ReadFlash(0x802D);
  B_2[1] = ReadFlash(0x802E);
  B_3[1] = ReadFlash(0x802F);
  control_Temp = B_1[1];
  B_CH[1] = control_Temp << 16;
  control_Temp = B_2[1];
  B_CH[1] |= control_Temp << 8;
  B_CH[1] |= B_3[1];

  A_1[2] = ReadFlash(0x8030);
  A_2[2] = ReadFlash(0x8031);
  A_3[2] = ReadFlash(0x8032);
  control_Temp = A_1[2];
  A_CH[2] = control_Temp << 16;
  control_Temp = A_2[2];
  A_CH[2] |= control_Temp << 8;
  A_CH[2] |= A_3[2];

  B_1[2] = ReadFlash(0x8033);
  B_2[2] = ReadFlash(0x8034);
  B_3[2] = ReadFlash(0x8035);
  control_Temp = B_1[2];
  B_CH[2] = control_Temp << 16;
  control_Temp = B_2[2];
  B_CH[2] |= control_Temp << 8;
  B_CH[2] |= B_3[2];

  A_1[3] = ReadFlash(0x8036);
  A_2[3] = ReadFlash(0x8037);
  A_3[3] = ReadFlash(0x8038);
  control_Temp = A_1[3];
  A_CH[3] = control_Temp << 16;
  control_Temp = A_2[3];
  A_CH[3] |= control_Temp << 8;
  A_CH[3] |= A_3[3];

  B_1[3] = ReadFlash(0x8039);
  B_2[3] = ReadFlash(0x803A);
  B_3[3] = ReadFlash(0x803B);
  control_Temp = B_1[3];
  B_CH[3] = control_Temp << 16;
  control_Temp = B_2[3];
  B_CH[3] |= control_Temp << 8;
  B_CH[3] |= B_3[3];

  A_1[4] = ReadFlash(0x803C);
  A_2[4] = ReadFlash(0x803D);
  A_3[4] = ReadFlash(0x803E);
  control_Temp = A_1[4];
  A_CH[4] = control_Temp << 16;
  control_Temp = A_2[4];
  A_CH[4] |= control_Temp << 8;
  A_CH[4] |= A_3[4];

  B_1[4] = ReadFlash(0x803F);
  B_2[4] = ReadFlash(0x8040);
  B_3[4] = ReadFlash(0x8041);
  control_Temp = B_1[4];
  B_CH[4] = control_Temp << 16;
  control_Temp = B_2[4];
  B_CH[4] |= control_Temp << 8;
  B_CH[4] |= B_3[4];

  rco_fTsetCamara[0] = ReadFlash(0x8042);
  rco_fTsetCamara[1] = ReadFlash(0x8043);
  rco_fTsetCamara[2] = ReadFlash(0x8044);

  control_TSetCamaraN = (rco_fTsetCamara[0] << 16) | (rco_fTsetCamara[1] << 8) | rco_fTsetCamara[2];

  rco_fTsetAmostra[0] = ReadFlash(0x8045);
  rco_fTsetAmostra[1] = ReadFlash(0x8046);
  rco_fTsetAmostra[2] = ReadFlash(0x8047);

  control_TSetAmostraN = (rco_fTsetAmostra[0] << 16) | (rco_fTsetAmostra[1] << 8) | rco_fTsetAmostra[2];

  rco_fTsetPeltier[0] = ReadFlash(0x8048);
  rco_fTsetPeltier[1] = ReadFlash(0x8049);
  rco_fTsetPeltier[2] = ReadFlash(0x804A);

  control_TSetPeltierN = (rco_fTsetPeltier[0] << 16) | (rco_fTsetPeltier[1] << 8) | rco_fTsetPeltier[2];

  rco_fTsetExtra[0] = ReadFlash(0x804B);
  rco_fTsetExtra[1] = ReadFlash(0x804C);
  rco_fTsetExtra[2] = ReadFlash(0x804D);

  control_TSetExtraN = (rco_fTsetExtra[0] << 16) | (rco_fTsetExtra[1] << 8) | rco_fTsetExtra[2];

  for (i = 0; i < 35; i++)
  {
    plan_hourProg[i] = ReadFlash(0x804E + i);
    plan_minProg[i] = ReadFlash(0x8083 + i);
  }
  control_HistTemp[0] = ReadFlash(0x80A6);
  control_HistTemp[1] = ReadFlash(0x80A7);
  control_HistTemp[2] = ReadFlash(0x80A8);
  control_HistTemp[3] = ReadFlash(0x80A9);
  control_HistTemp[4] = ReadFlash(0x80AA);

  control_autoDefrostCamera[0] = ReadFlash(0x80AB);
  control_autoDefrostCamera[1] = ReadFlash(0x80AC);
  control_autoDefrostCamera[2] = ReadFlash(0x80AD);

  control_TdefrostCamaraN = (control_autoDefrostCamera[0] << 16) | (control_autoDefrostCamera[1] << 8) | control_autoDefrostCamera[2];

  control_autoDefrostPeltier[0] = ReadFlash(0x80AE);
  control_autoDefrostPeltier[1] = ReadFlash(0x80AF);
  control_autoDefrostPeltier[2] = ReadFlash(0x80B0);

  control_TdefrostPeltierN = (control_autoDefrostPeltier[0] << 16) | (control_autoDefrostPeltier[1] << 8) | control_autoDefrostPeltier[2];

  control_autoStandCamera[0] = ReadFlash(0x80B1);
  control_autoStandCamera[1] = ReadFlash(0x80B2);
  control_autoStandCamera[2] = ReadFlash(0x80B3);

  control_TstandCamaraN = (control_autoStandCamera[0] << 16) | (control_autoStandCamera[1] << 8) | control_autoStandCamera[2];

  control_autoStandPeltier[0] = ReadFlash(0x80B4);
  control_autoStandPeltier[1] = ReadFlash(0x80B5);
  control_autoStandPeltier[2] = ReadFlash(0x80B6);

  control_TstandPeltierN = (control_autoStandPeltier[0] << 16) | (control_autoStandPeltier[1] << 8) | control_autoStandPeltier[2];

  adc_Offset[0] = ReadFlash(0x80B7);
  adc_Offset[1] = ReadFlash(0x80B8);
  adc_Offset[2] = ReadFlash(0x80B9);
  adc_Offset[3] = ReadFlash(0x80BA);
  adc_Offset[4] = ReadFlash(0x80BB);

  operation_chCalibrado[0] = ReadFlash(0x80BC);
  operation_chCalibrado[1] = ReadFlash(0x80BD);
  operation_chCalibrado[2] = ReadFlash(0x80BE);
  operation_chCalibrado[3] = ReadFlash(0x80BF);
  operation_chCalibrado[4] = ReadFlash(0x80C0);

  plan_nbH = 0;
  plan_nbL = 0;

  flash_initialSum += display_Language;
  flash_initialSum += flash_userPassword[0];
  flash_initialSum += flash_userPassword[1];
  flash_initialSum += flash_userPassword[2];
  flash_initialSum += flash_userPassword[3];
  flash_initialSum += rco_numpulseAdjustment;
  flash_initialSum += rco_periodAdjustment;
  flash_initialSum += rco_trimValue[0];
  flash_initialSum += rco_sectionValue[0];
  flash_initialSum += rco_trimValue[1];
  flash_initialSum += rco_sectionValue[1];
  flash_initialSum += rco_trimValue[2];
  flash_initialSum += rco_sectionValue[2];
  flash_initialSum += rco_trimValue[3];
  flash_initialSum += rco_sectionValue[3];
  flash_initialSum += rco_trimValue[4];
  flash_initialSum += rco_sectionValue[4];
  flash_initialSum += rco_trimValue[5];
  flash_initialSum += rco_sectionValue[5];
  flash_initialSum += rco_trimValue[6];
  flash_initialSum += rco_sectionValue[6];
  flash_initialSum += control_RetracValue;

  //testar o initial Sum

  flash_initialSum += adc_Offset[0];
  flash_initialSum += adc_Offset[1];
  flash_initialSum += adc_Offset[2];
  flash_initialSum += adc_Offset[3];
  flash_initialSum += adc_Offset[4];

  flash_initialSum += A_1[0];
  flash_initialSum += A_2[0];
  flash_initialSum += A_3[0];
  flash_initialSum += B_1[0];
  flash_initialSum += B_2[0];
  flash_initialSum += B_3[0];

  flash_initialSum += A_1[1];
  flash_initialSum += A_2[1];
  flash_initialSum += A_3[1];
  flash_initialSum += B_1[1];
  flash_initialSum += B_2[1];
  flash_initialSum += B_3[1];

  flash_initialSum += A_1[2];
  flash_initialSum += A_2[2];
  flash_initialSum += A_3[2];
  flash_initialSum += B_1[2];
  flash_initialSum += B_2[2];
  flash_initialSum += B_3[2];

  flash_initialSum += A_1[3];
  flash_initialSum += A_2[3];
  flash_initialSum += A_3[3];
  flash_initialSum += B_1[3];
  flash_initialSum += B_2[3];
  flash_initialSum += B_3[3];

  flash_initialSum += A_1[4];
  flash_initialSum += A_2[4];
  flash_initialSum += A_3[4];
  flash_initialSum += B_1[4];
  flash_initialSum += B_2[4];
  flash_initialSum += B_3[4];

  flash_initialSum += rco_fTsetCamara[0];
  flash_initialSum += rco_fTsetCamara[1];
  flash_initialSum += rco_fTsetCamara[2];

  flash_initialSum += rco_fTsetAmostra[0];
  flash_initialSum += rco_fTsetAmostra[1];
  flash_initialSum += rco_fTsetAmostra[2];

  flash_initialSum += rco_fTsetPeltier[0];
  flash_initialSum += rco_fTsetPeltier[1];
  flash_initialSum += rco_fTsetPeltier[2];

  flash_initialSum += rco_fTsetExtra[0];
  flash_initialSum += rco_fTsetExtra[1];
  flash_initialSum += rco_fTsetExtra[2];

  for (i = 0; i < 35; i++)
  {
    flash_initialSum += plan_hourProg[i];
    flash_initialSum += plan_minProg[i];
  }

  flash_initialSum += control_HistTemp[0];
  flash_initialSum += control_HistTemp[1];
  flash_initialSum += control_HistTemp[2];
  flash_initialSum += control_HistTemp[3];
  flash_initialSum += control_HistTemp[4];

  flash_initialSum += control_autoDefrostCamera[0];
  flash_initialSum += control_autoDefrostCamera[1];
  flash_initialSum += control_autoDefrostCamera[2];

  flash_initialSum += control_autoDefrostPeltier[0];
  flash_initialSum += control_autoDefrostPeltier[1];
  flash_initialSum += control_autoDefrostPeltier[2];

  flash_initialSum += control_autoStandCamera[0];
  flash_initialSum += control_autoStandCamera[1];
  flash_initialSum += control_autoStandCamera[2];

  flash_initialSum += control_autoStandPeltier[0];
  flash_initialSum += control_autoStandPeltier[1];
  flash_initialSum += control_autoStandPeltier[2];

  flash_initialSum += operation_chCalibrado[0];
  flash_initialSum += operation_chCalibrado[1];
  flash_initialSum += operation_chCalibrado[2];
  flash_initialSum += operation_chCalibrado[3];
  flash_initialSum += operation_chCalibrado[4];

  while(flash_initialSum > 255){
    flash_initialSum = flash_initialSum - 255;
    flash_chinitialSum++;
  }

  flash_chcheckSum = ReadFlash(0x80FF);

  if (display_Language > 0x03 || flash_chinitialSum != flash_chcheckSum || flash_resetFlag == 1) //memoria apagada  ou flash_checkSum inconsistente
  {
    flash_resetFlag = 0;
    display_Language = 0x00;
    flash_userPassword[0] = 0x05;
    flash_userPassword[1] = 0x05;
    flash_userPassword[2] = 0x05;
    flash_userPassword[3] = 0x05;
    rco_numpulseAdjustment = 10; //deslocamento fator multiplicador = 10 com microstep do drive em 128
    rco_periodAdjustment = 5;    //velocidade  255 menos o periodo pulso
    rco_trimValue[0] = 0;
    rco_sectionValue[0] = 0;
    rco_trimValue[1] = 0;
    rco_sectionValue[1] = 0;
    rco_trimValue[2] = 0;
    rco_sectionValue[2] = 0;
    rco_trimValue[3] = 0;
    rco_sectionValue[3] = 0;
    rco_trimValue[4] = 0;
    rco_sectionValue[4] = 0;
    rco_trimValue[5] = 0;
    rco_sectionValue[5] = 0;
    rco_trimValue[6] = 0;
    rco_sectionValue[6] = 0;
    control_RetracValue = 5;

    rco_fTsetAmostra[0] = 0xFF;
    rco_fTsetAmostra[1] = 0xFF;
    rco_fTsetAmostra[2] = 0x9C;

    rco_fTsetPeltier[0] = 0xFF;
    rco_fTsetPeltier[1] = 0xFF;
    rco_fTsetPeltier[2] = 0x9C;

    rco_fTsetCamara[0] = 0xFF;
    rco_fTsetCamara[1] = 0xFF;
    rco_fTsetCamara[2] = 0x9C;

    rco_fTsetExtra[0] = 0xFF;
    rco_fTsetExtra[1] = 0xFF;
    rco_fTsetExtra[2] = 0x9C;

    control_autoDefrostCamera[0] = 0xFF;
    control_autoDefrostCamera[1] = 0xFF;
    control_autoDefrostCamera[2] = 0x9C;

    control_autoDefrostPeltier[0] = 0xFF;
    control_autoDefrostPeltier[1] = 0xFF;
    control_autoDefrostPeltier[2] = 0x9C;

    control_autoStandCamera[0] = 0xFF;
    control_autoStandCamera[1] = 0xFF;
    control_autoStandCamera[2] = 0x9C;

    control_autoStandPeltier[0] = 0xFF;
    control_autoStandPeltier[1] = 0xFF;
    control_autoStandPeltier[2] = 0x9C;

    control_TSetCamaraN = -10;
    control_TSetPeltierN = -10;
    control_TSetAmostraN = -10;
    control_TSetExtraN = -10;
    control_TdefrostCamaraN = -10;
    control_TdefrostPeltierN = -10;
    control_TstandCamaraN = -10;
    control_TstandPeltierN = -10;

    operation_chCalibrado[0] = 0;
    operation_chCalibrado[1] = 0;
    operation_chCalibrado[2] = 0;
    operation_chCalibrado[3] = 0;
    operation_chCalibrado[4] = 0;

    plan_minProgTemp = 0xFF;
    plan_hourProgTemp = 0xFF;

    j = 0;
    for (i = 0; i < 35; i++)
    {
      if (j == 0) //Defrost
      {
        plan_minProg[i] = 14;
        plan_hourProg[i] = 16;
        j++;
      }
      else if (j == 1) //O3
      {
        plan_minProg[i] = 15;
        plan_hourProg[i] = 16;
        j++;
      }
      else if (j == 2) //UV
      {
        plan_minProg[i] = 16;
        plan_hourProg[i] = 16;
        j++;
      }
      else if (j == 3) //Sleep
      {
        plan_minProg[i] = 17;
        plan_hourProg[i] = 16;
        j++;
      }
      else if (j == 4) //WAKEup
      {
        plan_minProg[i] = 13;
        plan_hourProg[i] = 16;
        j = 0;
      }
    }
    control_HistTemp[0] = 0;
    control_HistTemp[1] = 0;
    control_HistTemp[2] = 0;
    control_HistTemp[3] = 0;
    control_HistTemp[4] = 0;

    adc_Offset[0] = 48;
    adc_Offset[1] = 48;
    adc_Offset[2] = 48;
    adc_Offset[3] = 48;
    adc_Offset[4] = 48;

    flash_updateStatus();
  }

  // Apaga a FLASH do endereço 0x1860 até a endereço 0x1FFF
  // res = flash_page_erase(0x8000); // 416 bytes
  // res = flash_page_erase(0x81A0); // 512 bytes
  // res = flash_page_erase(0x83A0); // 512 bytes
  // res = flash_page_erase(0x85A0); // 512 bytes
}

void flash_updateDefrost(void)
{
  control_TdefrostPeltierN = operation_TdefrostPeltierN;
  control_Temp = control_TdefrostPeltierN >> 16;
  control_autoDefrostPeltier[0] = control_Temp;
  control_Temp = control_TdefrostPeltierN >> 8;
  control_autoDefrostPeltier[1] = control_Temp;
  control_autoDefrostPeltier[2] = control_TdefrostPeltierN;

  control_TdefrostCamaraN = operation_TdefrostCamaraN;
  control_Temp = control_TdefrostCamaraN >> 16;
  control_autoDefrostCamera[0] = control_Temp;
  control_Temp = control_TdefrostCamaraN >> 8;
  control_autoDefrostCamera[1] = control_Temp;
  control_autoDefrostCamera[2] = control_TdefrostCamaraN;

  flash_updateStatus();
}

void flash_updateStand(void)
{
  control_TstandPeltierN = operation_TstandPeltierN;
  control_Temp = control_TstandPeltierN >> 16;
  control_autoStandPeltier[0] = control_Temp;
  control_Temp = control_TstandPeltierN >> 8;
  control_autoStandPeltier[1] = control_Temp;
  control_autoStandPeltier[2] = control_TstandPeltierN;

  control_TstandCamaraN = operation_TstandCamaraN;
  control_Temp = control_TstandCamaraN >> 16;
  control_autoStandCamera[0] = control_Temp;
  control_Temp = control_TstandCamaraN >> 8;
  control_autoStandCamera[1] = control_Temp;
  control_autoStandCamera[2] = control_TstandCamaraN;

  flash_updateStatus();
}

void flash_updateNormalTemp(void)
{
  control_TSetCamaraN = operation_TSetCamaraN;
  control_Temp = control_TSetCamaraN >> 16;
  rco_fTsetCamara[0] = control_Temp;
  control_Temp = control_TSetCamaraN >> 8;
  rco_fTsetCamara[1] = control_Temp;
  rco_fTsetCamara[2] = control_TSetCamaraN;

  control_TSetPeltierN = operation_TSetPeltierN;
  control_Temp = control_TSetPeltierN >> 16;
  rco_fTsetPeltier[0] = control_Temp;
  control_Temp = control_TSetPeltierN >> 8;
  rco_fTsetPeltier[1] = control_Temp;
  rco_fTsetPeltier[2] = control_TSetPeltierN;

  flash_updateStatus();
}