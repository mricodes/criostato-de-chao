/*===============================================================================================
/	EQUIPAMENTO: Placa LPCPU02
/	EMPRESA: LUPETEC
/   Cabecalho do Clock
===============================================================================================*/

//Includes 
#include "MC9S08AC32.h"

//Clock configuration defines
#define SOPT_CONFIG 0x53
#define SPMSC1_CONFIG 0x1C
#define SPMSC2_CONFIG 0x00
#define SMCLK_CONFIG 0
#define ICGC1_CONFIG 0xFC
#define ICGC2_CONFIG 0x30


//Clock function
void clock_Init(void);