/*===============================================================================================
/	EQUIPAMENTO: Placa LPCPU02
/	EMPRESA: LUPETEC
/   Cabecalho para a operacao
===============================================================================================*/

//Defines

//Macros Sensores
#define fimCurso0 PTDD_PTDD0
#define fimCurso1 PTDD_PTDD1

#define fimTampa PTFD_PTFD4

#define opticoBaixo PTDD_PTDD2
#define opticoCima PTDD_PTDD3
#define opticoVolante PTDD_PTDD4

#define STATE_OPERATION 0x01
#define STATE_ADJUST 0x02
#define STATE_CONFIG 0x03
#define STATE_RCO_WAIT 0x00

#define STATE_PLAN_DEFROST 0x04
#define STATE_PLAN_AUTO 0x05
#define STATE_PLAN_DESCON 0x06
#define STATE_PLAN_PRESET 0x07

#define STATE_CONFIG_IDIOME 0x08
#define STATE_CONFIG_DATA 0x09
#define STATE_CONFIG_CAL 0x0A
#define STATE_CONFIG_TESTS 0x0B
#define STATE_CONFIG_FACTORY 0x0C
#define STATE_CONFIG_PASSWORD 0x0D
#define STATE_CONFIG_ABOUT 0x0E

#define STATE_AUTO_RETRO 0x12

#define STATE_PASSWORD 0x10
#define STATE_PASSWORD_CONF 0x10

#define STATE_ASK 0x11

//States definition
#define STATE_AUTO_NORMAL 0x01
#define STATE_AUTO_BLOCKED 0x02
#define STATE_AUTO_STAND 0x03
#define STATE_AUTO_DEFROST 0x04
#define STATE_AUTO_UV 0x05
#define STATE_AUTO_OZONIO 0x06

//Macro Keys
//Side Menu
#define OPERATION_KEY 0x21
#define ADJUST_KEY 0x1F
#define CONFIG_KEY 0x20

//Operation Screen
#define OPERATION_KEY_INTERNA 0x19
#define OPERATION_KEY_UV 0x1B
#define OPERATION_KEY_OZONIO 0x25
#define OPERATION_KEY_DEFROST 0x1C
#define OPERATION_KEY_STAND 0x1E
#define OPERATION_KEY_BLOCK 0x22

#define OPERATION_KEY_TRIM 0x14
#define OPERATION_KEY_CUT_SECTION 0x15
#define OPERATION_KEY_SLOW_ADVANCE 0x10
#define OPERATION_KEY_FAST_ADVANCE 0x12
#define OPERATIN_KEY_SLOW_RETREAT 0x11
#define OPERATION_KEY_FAST_RETREAT 0x13
#define OPERATION_KEY_MICRA_DOWN 0x17
#define OPERATION_KEY_MICRA_UP 0x16
#define OPERATION_KEY_MICRA_ZERO 0x18
#define OPERATION_KEY_TEMP_CONFIG 0x23
#define OPERATION_KEY_TEMP_UP 0x24
#define OPERATION_KEY_SIM 0x26
#define OPERATION_KEY_TOTAL_RETREAT 0x1D
#define OPERATION_KEY_CLOSE_POPUP 0x2E

//Adjust Screen
#define ADJUST_KEY_DEFROST 0x30
#define ADJUST_KEY_AUTO 0x31
#define ADJUST_KEY_DESCONTA 0x32

//Adjust Screen - Programming
#define ADJUST_KEY_HOUR_UP 0x66
#define ADJUST_KEY_HOUR_DOWN 0x65
#define ADJUST_KEY_MIN_UP 0x68
#define ADJUST_KEY_MIN_DOWN 0x67
#define ADJUST_KEY_SAVE 0x6A
#define ADJUST_KEY_CANCEL 0x69

//Adjust screen defines
#define ADJUST_KEY_SAMPLE_DOWN 0x35
#define ADJUST_KEY_SAMPLE_UP 0x36
#define ADJUST_KEY_PELTIER_DOWN 0x37
#define ADJUST_KEY_PELTIER_UP 0x38
#define ADJUST_KEY_PRESET 0x39
#define ADJUST_KEY_RETRA_DOWN 0x41
#define ADJUST_KEY_RETRA_UP 0x42
#define ADJUST_KEY_AUTO_PROG 0x31
#define ADJUST_KEY_SAVE_TEMP 0x40
#define BtADesconta 0x32

//Icons
#define ICON_TRIM 0x51
#define ICON_SEC 0x52
#define ICON_INTERNA 0x50
#define ICON_UV 0x53
#define ICON_OZONIO 0x54
#define ICON_STAND 0x55
#define ICON_DEFROST 0x56
#define ICON_BLOCK 0x57
#define ICON_VOLANTE 0x58

//Configuration Screen
#define CONFIG_KEY_IDIOME 0x30
#define CONFIG_KEY_DATA 0x31
#define CONFIG_KEY_CALIBRATION 0x32
#define CONFIG_KEY_TESTS 0x33
#define CONFIG_KEY_FACTORY 0x34
#define CONFIG_KEY_PASSWORD 0x35
#define CONFIG_KEY_ABOUT 0x36
#define CONFIG_KEY_AUTO_ADJUST 0x37
#define CONFIG_KEY_SIM 0x26

//Configuration automatic adjust Screen
#define CONFIG_KEY_A_PELTIER_DOWN 0x30
#define CONFIG_KEY_A_PELTIER_UP 0x31

//////Tela Testes Manutenção
#define BtTCompressor 0x22
#define BtTPeltier 0x23
#define BtTUV 0x24
#define BtTInterna 0x25
#define BtTOzonio 0x26
#define BtTFog 0x27
#define BtTExtra 0x28
#define BtTSentido1 0x29
#define BtTSentido2 0x2A
#define BtTHabilitar 0x2B

//////Tela Calibracao
#define BtPosChMenos 0x30
#define BtPosChMais 0x31
#define BtPos120R 0x32
#define BtPos96R 0x33
#define BtNegChMenos 0x34
#define BtNegChMais 0x35
#define BtNeg100R 0x36
#define BtNeg75R 0x37
#define BtNegHistMenos 0x3B
#define BtNegHistMais 0x3A
#define BtPosHistMenos 0x39
#define BtPosHistMais 0x38

//Temperatura
#define TEMPERATURE_AMBIENT 4
#define TEMPERATURE_CHAMBER 3
#define TEMPERATURE_SAMPLE 2
#define TEMPERATURE_PELTIER 1
#define TEMPERATURE_EXTRA 0
#define TEMPERATURE_SAMPLE_UPDATE 10
#define TEMPERATURE_PELTIER_UPDATE 11
#define OPERATION_TEMPERATURE_PELTIER_UPDATE 12
#define TEMPERATURE_CHAMBER_UPDATE 13
#define OPERATION_TEMPERATURE_CHAMBER_UPDATE 14
#define TEMPERATURE_DEFROST_AUTO_PELTIER 15
#define TEMPERATURE_DEFROST_AUTO_CHAMBER 16
#define TEMPERATURE_STAND_AUTO_PELTIER 17
#define TEMPERATURE_STAND_AUTO_CHAMBER 18
#define TEMPERATURE_OFFSET 19


//Display
#define POPFIMCURSO 0x01
#define POPRETROMOTOR 0x02
#define POPVOLANTE 0x03
#define POPBLOCK 0x04
#define POPCONGELA 0x05
#define POPDEFROST 0x06
#define POPDESCONTA 0x07
#define POPPROSSEGUE 0x08
#define POPMOTORTOTAL 0x09
#define POPSTAND 0x0A
#define POPSENHAINV 0x0B

extern long int operation_TSetPeltierN;
extern long int operation_TSetCamaraN;

extern long int operation_TdefrostCamaraN;
extern long int operation_TdefrostPeltierN;
extern long int operation_TstandCamaraN;
extern long int operation_TstandPeltierN;

extern unsigned char operation_chCalibrado[5];

//Functions
void operation_trimAdd(void);
void operation_trimRemove(void);
void operation_sectionAdd(void);
void operation_sectionRemove(void);
void operation_tempcamaraUp(void);
void operation_tempcamaraDown(void);
void operation_temppeltierUp(void);
void operation_temppeltierDown(void);
void operation_tempsampleUp(void);
void operation_tempsampleDown(void);
void operation_retraUp(void);
void operation_retraDown(void);
void operation_standardH(unsigned char Channel);
void operation_Coefficient(unsigned char Channel, unsigned char Band);
void operation_TemperatureUpdate(void);
void operation_Loop(void);

//Automatic states temperature set
void operation_chamberStandUp(void);
void operation_chamberStandDown(void);
void operation_chamberDefrostUp(void);
void operation_chamberDefrostDown(void);
void operation_peltierStandUp(void);
void operation_peltierStandDown(void);
void operation_peltierDefrostUp(void);
void operation_peltierDefrostDown(void);