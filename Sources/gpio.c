/*===============================================================================================
/	  EQUIPAMENTO: Placa LPCPU02
/	  EMPRESA: LUPETEC
/   Rotinas de configuracao de pinos de entrada e saida
===============================================================================================*/

#include "gpio.h"

void gpio_Init(void)
{
  //PORTA bits configuration
  PTADD = 0b11111111;
  PTASE &= (unsigned char)~0x03;
  PTADS = 0x00;

  //PORTB bits configuration
  PTBDD = 0b00000111;
  PTBSE &= (unsigned char)~0x0F;
  PTBDS = 0x00;

  //PORTC bits configuration
  PTCDD = 0b11011111;
  PTCSE = (PTCSE & (unsigned char)~0x38) | (unsigned char)0x07;
  PTCDS = 0x00;

  //PORTD bits configuration
  PTDDD = 0b11100000;
  PTDSE &= (unsigned char)~0x0F;
  PTDDS = 0x00;

  //PORTE bits configuration
  PTEDD = 0b11111101;
  PTESE = 0x00;
  PTEDS = 0x00;

  //PORTF bits configuration
  PTFDD = 0b11100000;
  PTFSE &= (unsigned char)~0x33;
  PTFDS = 0x00;

  //PORTG bits configuration
  PTGDD = 0b11110111;
  PTGSE &= (unsigned char)~0x6F;
  PTGDS = 0x00;
  PTGDD = 0xFF;
}