/*===============================================================================================
/	  EQUIPAMENTO: Placa LPCPU02
/	  EMPRESA: LUPETEC
/   Cabecalho do conversor analogico-digital
===============================================================================================*/

//ADC Includes
#include "MC9S08AC32.h"

#define MEDIA_ADC 64

//ADC variables
extern unsigned int adc_Sum;
extern unsigned int adc_Value[5];
extern unsigned char adc_Offset[5];
extern unsigned char adc_cntMedia;
extern unsigned char adc_auxConfig;
extern unsigned char indexADC;
extern long int adc_Taux;
extern long int adc_Taux1;
extern long int adc_TOut;
extern long int adc_T1;
extern long int adc_T2;

extern unsigned char cntMediaADC;
extern unsigned int somaADC;

void adc_Init(void);