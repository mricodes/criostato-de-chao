/*===============================================================================================
/	  EQUIPAMENTO: Placa LPCPU02
/	  EMPRESA: LUPETEC
/     Funcao de comunicao com operador
===============================================================================================*/

#include "rco.h"
#include "systemkernel.h"

void rco_Loop(void)
{
    switch (rco_ScreenState)
    {

    case WAIT_SCREEN:

        //Initial configuration start
        if (timer2_Delay(10))
        {
            //Change rco state machine
            rco_ScreenState = OPERATION_SCREEN;

            adc_auxConfig = 0b01000111;
            ADC1SC1 = adc_auxConfig;

            //Initialize Memory
            flash_Init();

            //Set 0 to automatic advance
            control_advanceAuto = 0;

            //Set operation camara temperature and peltier
            operation_TSetCamaraN = control_TSetCamaraN;
            operation_TSetPeltierN = control_TSetPeltierN;
            operation_TstandPeltierN = control_TstandPeltierN;
            operation_TdefrostPeltierN = control_TdefrostPeltierN;
            operation_TstandCamaraN = control_TstandCamaraN;
            operation_TdefrostCamaraN = control_TdefrostCamaraN;

            //Activate 10ms buzzer
            display_Buzzer(30);

            //Activate icon update
            display_iconFlag = 1;

            //Set automatic state to normal
            rco_autoState = STATE_AUTO_NORMAL;

            //Disable blocked state
            rco_blockedState = 0;

            //Update time field
            display_rtcRead();

            //Reset time value
            display_timeReset();
        }

        break;

    case OPERATION_SCREEN:

        rco_operationState();

        break;

        ////////////////////////////Adjust screen state

    case ADJUST_SCREEN:

        rco_adjustState();

        break;

    case CONFIG_SCREEN:

        rco_configState();

        break;
    }
}

void rco_operationState(void)
{
    //Set password state
    rco_passwordState = 0x00;

    //Load main operation screen
    display_loadScreen(DISPLAY_OPERATION_MAIN_SCREEN);

    //Set 2 to automatic advance
    control_advanceAuto = 2;

    if (TecLateral)
    {
        if (TecAvancoLento)
        {
            display_Buzzer(30);

            rco_periodAdjustment = 6;
            rco_numpulseAdjustment = 15;
            control_Micras = 250;

            if (!control_motorAdvance())
            {
                display_loadPopup(POPFIMCURSO);
            }
        }
        else if (TecAvancoRapido)
        {
            display_Buzzer(30);

            rco_periodAdjustment = 1;
            rco_numpulseAdjustment = 38;
            control_Micras = 250;

            if (!control_motorAdvance())
            {
                display_loadPopup(POPFIMCURSO);
            }
        }
        else if (TecRetroLento)
        {
            display_Buzzer(30);

            rco_periodAdjustment = 6;
            rco_numpulseAdjustment = 15;
            control_Micras = 250;

            if (!control_motorRetreat())
            {
                display_loadPopup(POPFIMCURSO);
            }
        }
        else if (TecRetroRapido)

        {
            display_Buzzer(30);

            rco_periodAdjustment = 1;
            rco_numpulseAdjustment = 48;
            control_Micras = 250;

            if (!control_motorRetreat())
            {
                display_loadPopup(POPFIMCURSO);
            }
        }
    }

    if (display_Touch())
    {

        display_iconFlag = 1;
        switch (display_keyValue)
        {

        case OPERATION_KEY_INTERNA:

            control_StOutInterna ^= 0x01;
            break;

        case OPERATION_KEY_UV:
            control_StOutUV ^= 0x01;
            break;

        case OPERATION_KEY_OZONIO:
            control_StOutOzonio ^= 0x01;
            break;

        case OPERATION_KEY_DEFROST:

            switch (rco_autoState)
            {
            case STATE_AUTO_NORMAL:

                //Load Defrost popup
                display_loadPopup(POPDEFROST);

                //Set auto defrost to temperature auto state
                rco_tempautoState = STATE_AUTO_DEFROST;
                break;

            case STATE_AUTO_DEFROST:

                rco_autoState = STATE_AUTO_NORMAL;
                control_StOutAdicional = 0x00;

                break;
            }

            break;

        case OPERATION_KEY_STAND:
            switch (rco_autoState)
            {
            case STATE_AUTO_NORMAL:

                //Load Defrost popup
                display_loadPopup(POPSTAND);

                //Set auto defrost to temperature auto state
                rco_tempautoState = STATE_AUTO_STAND;
                break;

            case STATE_AUTO_STAND:

                rco_autoState = STATE_AUTO_NORMAL;

                break;
            }

            break;

        case OPERATION_KEY_BLOCK:
            if (rco_blockedState != STATE_AUTO_BLOCKED)
            {

                display_loadPopup(POPBLOCK);
                rco_tempautoState = STATE_AUTO_BLOCKED;
            }
            else
            {
                rco_blockedState = 0;
            }
            break;

        case OPERATION_KEY_TOTAL_RETREAT:

            if (fimCurso1)
            {
                display_loadPopup(POPFIMCURSO);
            }
            else
            {
                display_loadPopup(POPMOTORTOTAL);
                rco_tempautoState = STATE_AUTO_RETRO;
            }
            break;

        case OPERATION_KEY_SIM:

            if (rco_tempautoState != STATE_AUTO_BLOCKED)
            {
                rco_lastautoState = rco_autoState;
                rco_autoState = rco_tempautoState;
            }
            else
            {
                rco_blockedState = rco_tempautoState;
            }
            break;

        case OPERATION_KEY_SLOW_ADVANCE:

            rco_periodAdjustment = 6;
            rco_numpulseAdjustment = 15;
            control_Micras = 250;

            if (!control_motorAdvance())
            {
                display_loadPopup(POPFIMCURSO);
            }

            break;

        case OPERATION_KEY_FAST_ADVANCE:

            rco_periodAdjustment = 1;
            rco_numpulseAdjustment = 38;
            control_Micras = 250;

            if (!control_motorAdvance())
            {
                display_loadPopup(POPFIMCURSO);
            }

            break;

        case OPERATIN_KEY_SLOW_RETREAT:

            rco_periodAdjustment = 6;
            rco_numpulseAdjustment = 15;
            control_Micras = 250;

            if (!control_motorRetreat())
            {
                display_loadPopup(POPFIMCURSO);
            }

            break;

        case OPERATION_KEY_FAST_RETREAT:

            rco_periodAdjustment = 1;
            rco_numpulseAdjustment = 48;
            control_Micras = 250;

            if (!control_motorRetreat())
            {
                display_loadPopup(POPFIMCURSO);
            }

            break;

        case OPERATION_KEY_TRIM:

            rco_StTrim = 0x01;

            break;

        case OPERATION_KEY_CUT_SECTION:

            rco_StTrim = 0x00;
            break;

        case OPERATION_KEY_MICRA_UP:

            switch (rco_StTrim)
            {
            case 0x01:

                //Increment trim section
                operation_trimAdd();

                break;

            case 0x00:

                //Increment cut section
                operation_sectionAdd();

                break;
            }

            break;

        case OPERATION_KEY_MICRA_DOWN:

            switch (rco_StTrim)
            {

            case 0x01:

                //Remove trim section
                operation_trimRemove();

                break;
            case 0x00:

                //Remove cut section
                operation_sectionRemove();

                break;
            }

            break;

        case OPERATION_KEY_TEMP_CONFIG:

            //Decrease temperature
            rco_ScreenState = ADJUST_SCREEN;

            break;

        case OPERATION_KEY_MICRA_ZERO:

            rco_totalCut = 0;
            rco_totalMicras = 0;

            break;

        case OPERATION_KEY_CLOSE_POPUP:

            display_loadPopup(0x0D);

            break;

        case OPERATION_KEY:

            rco_ScreenState = OPERATION_SCREEN;

            break;

        case ADJUST_KEY:

            rco_ScreenState = ADJUST_SCREEN;
            rco_AdjustscreenState = ADJUST_SCREEN;
            break;

        case CONFIG_KEY:

            rco_ScreenState = CONFIG_SCREEN;
            rco_ConfigscreenState = CONFIG_SCREEN;

            break;
        }
        display_keyValue = 0xFF;
    }
}

void rco_adjustState(void)
{

    switch (rco_AdjustscreenState)
    {
    case ADJUST_SCREEN:
        //Set password state
        rco_passwordState = 0x00;

        //Load main operation screen
        display_loadScreen(DISPLAY_ADJUST_MAIN_SCREEN);

        control_advanceAuto = 0;

        if (display_Touch())
        {
            display_iconFlag = 1;

            switch (display_keyValue)
            {

            case ADJUST_KEY_SAMPLE_DOWN:

                //operation_tempsampleDown();

                break;

            case ADJUST_KEY_SAMPLE_UP:

                //operation_tempsampleUp();

                break;

            case ADJUST_KEY_PELTIER_DOWN:

                operation_temppeltierDown();

                break;

            case ADJUST_KEY_PELTIER_UP:

                operation_temppeltierUp();

                break;

            case 0x43:

                operation_tempcamaraDown();

                break;

            case 0x44:

                operation_tempcamaraUp();

                break;

            case ADJUST_KEY_RETRA_UP:

                operation_retraUp();

                break;

            case ADJUST_KEY_RETRA_DOWN:

                operation_retraDown();

                break;

            case ADJUST_KEY_AUTO_PROG:

                if (control_temperatureFlag)
                {
                    control_TSetCamaraN = operation_TSetCamaraN;
                    control_TSetPeltierN = operation_TSetPeltierN;
                }
                else
                {
                    operation_TSetCamaraN = control_TSetCamaraN;
                    operation_TSetPeltierN = control_TSetPeltierN;
                }

                rco_AdjustscreenState = ADJUST_PROG_AUTO_SCREEN;
                plan_ProgUpdate = 1;

                break;

            case ADJUST_KEY_PRESET:

                if (control_temperatureFlag)
                {
                    control_TSetCamaraN = operation_TSetCamaraN;
                    control_TSetPeltierN = operation_TSetPeltierN;
                }
                else
                {
                    operation_TSetCamaraN = control_TSetCamaraN;
                    operation_TSetPeltierN = control_TSetPeltierN;
                }

                rco_AdjustscreenState = ADJUST_PRESET_SCREEN;

                break;

            case ADJUST_KEY_SAVE_TEMP:

                control_temperatureFlag = 1;

                flash_updateNormalTemp();

                break;

            case OPERATION_KEY:

                if (control_temperatureFlag)
                {
                    control_TSetCamaraN = operation_TSetCamaraN;
                    control_TSetPeltierN = operation_TSetPeltierN;
                }
                else
                {
                    operation_TSetCamaraN = control_TSetCamaraN;
                    operation_TSetPeltierN = control_TSetPeltierN;
                }

                rco_ScreenState = OPERATION_SCREEN;

                break;

            case ADJUST_KEY:

                if (control_temperatureFlag)
                {
                    control_TSetCamaraN = operation_TSetCamaraN;
                    control_TSetPeltierN = operation_TSetPeltierN;
                }
                else
                {
                    operation_TSetCamaraN = control_TSetCamaraN;
                    operation_TSetPeltierN = control_TSetPeltierN;
                }

                rco_ScreenState = ADJUST_SCREEN;
                rco_AdjustscreenState = ADJUST_SCREEN;

                break;

            case CONFIG_KEY:

                if (control_temperatureFlag)
                {
                    control_TSetCamaraN = operation_TSetCamaraN;
                    control_TSetPeltierN = operation_TSetPeltierN;
                }
                else
                {
                    operation_TSetCamaraN = control_TSetCamaraN;
                    operation_TSetPeltierN = control_TSetPeltierN;
                }

                rco_ScreenState = CONFIG_SCREEN;
                rco_ConfigscreenState = CONFIG_SCREEN;

                break;
            }
            display_keyValue = 0xFF;
        }
        break;

    case ADJUST_PROG_AUTO_SCREEN:

        display_loadScreen(DISPLAY_ADJUST_PROG_AUTO_SCREEN);

        if (display_Touch())
        {
            display_iconFlag = 1;

            switch (display_keyValue)
            {

            case ADJUST_KEY_HOUR_UP:

                plan_hourUp();

                break;

            case ADJUST_KEY_HOUR_DOWN:

                plan_hourDown();

                break;

            case ADJUST_KEY_MIN_UP:

                plan_minUp();

                break;

            case ADJUST_KEY_MIN_DOWN:

                plan_minDown();

                break;

            case ADJUST_KEY_SAVE:

                plan_ProgUpdate = 1;

                plan_hourProg[plan_indexProg] = plan_hourProgTemp;
                plan_minProg[plan_indexProg] = plan_minProgTemp;

                flash_updateStatus();

                break;

            case ADJUST_KEY_CANCEL:

                plan_ProgUpdate = 2;
                plan_hourProgTemp = plan_hourProg[plan_indexProg];
                plan_minProgTemp = plan_minProg[plan_indexProg];

                break;

            case OPERATION_KEY + 0x90:

                rco_ScreenState = OPERATION_SCREEN;
                rco_AdjustscreenState = ADJUST_SCREEN;

                break;

            case ADJUST_KEY + 0x90:

                rco_ScreenState = ADJUST_SCREEN;
                rco_AdjustscreenState = ADJUST_SCREEN;

                break;

            case CONFIG_KEY + 0x90:

                rco_ScreenState = CONFIG_SCREEN;
                rco_AdjustscreenState = ADJUST_SCREEN;
                rco_ConfigscreenState = CONFIG_SCREEN;

                break;
            }

            if (display_keyValue < 0x65)
            {
                plan_ProgUpdate = 2;
                plan_nbH = (display_keyValue >> 4) & 0x0F;
                plan_nbL = display_keyValue & 0x0F;
                plan_indexProg = plan_nbH * 5 + plan_nbL;
                plan_hourProgTemp = plan_hourProg[plan_indexProg];
                plan_minProgTemp = plan_minProg[plan_indexProg];
            }

            display_keyValue = 0xFF;
        }

        break;

    case ADJUST_PRESET_SCREEN:

        display_loadScreen(DISPLAY_PROG_PRESET_SCREEN);
        control_advanceAuto = 0;
        if (display_Touch())
        {
            display_iconFlag = 1;

            switch (display_keyValue)
            {

            case 0x30:

                rco_trimValue[0] = rco_trimValue[1];
                rco_sectionValue[0] = rco_sectionValue[1];
                flash_updateStatus();
                display_Buzzer(30);

                break;

            case 0x31:

                rco_trimValue[0] = rco_trimValue[2];
                rco_sectionValue[0] = rco_sectionValue[2];
                flash_updateStatus();
                display_Buzzer(30);

                break;

            case 0x32:

                rco_trimValue[0] = rco_trimValue[3];
                rco_sectionValue[0] = rco_sectionValue[3];
                flash_updateStatus();
                display_Buzzer(30);

                break;

            case 0x33:

                rco_trimValue[0] = rco_trimValue[4];
                rco_sectionValue[0] = rco_sectionValue[4];
                flash_updateStatus();
                display_Buzzer(30);

                break;

            case 0x34:

                rco_trimValue[0] = rco_trimValue[5];
                rco_sectionValue[0] = rco_sectionValue[5];
                flash_updateStatus();
                display_Buzzer(30);
                break;

            case 0x35:

                rco_trimValue[0] = rco_trimValue[6];
                rco_sectionValue[0] = rco_sectionValue[6];
                flash_updateStatus();
                display_Buzzer(30);
                break;

                break;

            case 0x36:

                rco_trimValue[1] = rco_trimValue[0];
                rco_sectionValue[1] = rco_sectionValue[0];
                flash_updateStatus();
                display_Buzzer(30);

                break;

            case 0x37:

                rco_trimValue[2] = rco_trimValue[0];
                rco_sectionValue[2] = rco_sectionValue[0];
                flash_updateStatus();
                display_Buzzer(30);

                break;

            case 0x38:
                rco_trimValue[3] = rco_trimValue[0];
                rco_sectionValue[3] = rco_sectionValue[0];
                flash_updateStatus();
                display_Buzzer(30);
                break;

            case 0x39:

                rco_trimValue[4] = rco_trimValue[0];
                rco_sectionValue[4] = rco_sectionValue[0];
                flash_updateStatus();
                display_Buzzer(30);

                break;

            case 0x3A:

                rco_trimValue[5] = rco_trimValue[0];
                rco_sectionValue[5] = rco_sectionValue[0];
                flash_updateStatus();
                display_Buzzer(30);

                break;

            case 0x3B:

                rco_trimValue[6] = rco_trimValue[0];
                rco_sectionValue[6] = rco_sectionValue[0];
                flash_updateStatus();
                display_Buzzer(30);

                break;

            case OPERATION_KEY:

                rco_ScreenState = OPERATION_SCREEN;

                break;

            case ADJUST_KEY:

                rco_ScreenState = ADJUST_SCREEN;
                rco_AdjustscreenState = ADJUST_SCREEN;

                break;

            case CONFIG_KEY:

                rco_ScreenState = CONFIG_SCREEN;
                rco_ConfigscreenState = CONFIG_SCREEN;

                break;
            }
        }
        display_keyValue = 0xFF;

        display_fieldUpdate(0x01, rco_trimValue[1]);
        display_fieldUpdate(0x04, rco_sectionValue[1]);
        display_fieldUpdate(0x0A, rco_trimValue[2]);
        display_fieldUpdate(0x0D, rco_sectionValue[2]);
        display_fieldUpdate(0x13, rco_trimValue[3]);
        display_fieldUpdate(0x16, rco_sectionValue[3]);
        display_fieldUpdate(0x1C, rco_trimValue[4]);
        display_fieldUpdate(0x1F, rco_sectionValue[4]);
        display_fieldUpdate(0x25, rco_trimValue[5]);
        display_fieldUpdate(0x28, rco_sectionValue[5]);
        display_fieldUpdate(0x2E, rco_trimValue[6]);
        display_fieldUpdate(0x31, rco_sectionValue[6]);

        break;
    }
}

void rco_configState(void)
{

    switch (rco_ConfigscreenState)
    {

    case CONFIG_SCREEN:

        if (rco_passwordState == 0xAB)
        {
            display_loadScreen(DISPLAY_CONFIG_MAIN_SCREEN);
            control_advanceAuto = 0;
            if (display_Touch())
            {
                display_iconFlag = 1;

                switch (display_keyValue)
                {

                case CONFIG_KEY_IDIOME:

                    rco_ConfigscreenState = CONFIG_IDIOME_SCREEN;
                    display_languagePrevious = display_Language;

                    break;

                case CONFIG_KEY_DATA:

                    rco_ConfigscreenState = CONFIG_DATA_SCREEN;
                    display_rtcRead();

                    break;

                case CONFIG_KEY_CALIBRATION:

                    rco_ConfigscreenState = CONFIG_CALIBRATION_SCREEN;

                    break;

                case CONFIG_KEY_TESTS:

                    rco_ConfigscreenState = CONFIG_TESTS_SCREEN;

                    break;

                case CONFIG_KEY_FACTORY:
                    display_loadPopup(0x0C);

                    break;

                case CONFIG_KEY_SIM:
                    display_Buzzer(30);
                    display_loadScreen(0x00);
                    flash_resetFlag = 1;
                    rco_autoState = STATE_AUTO_NORMAL;
                    rco_ScreenState = WAIT_SCREEN;
                    rco_AdjustscreenState = ADJUST_SCREEN;
                    rco_ConfigscreenState = CONFIG_SCREEN;
                    timer2_Flag = STATE_TIMER_START;

                    break;

                case CONFIG_KEY_PASSWORD:

                    rco_ConfigscreenState = CONFIG_PASSWORD_SCREEN;
                    rco_contDigitoSenha = 0;
                    break;

                case CONFIG_KEY_ABOUT:

                    rco_ConfigscreenState = CONFIG_ABOUT_SCREEN;

                    break;

                case CONFIG_KEY_AUTO_ADJUST:

                    rco_ConfigscreenState = CONFIG_AUTO_ADJUST_SCREEN;

                    break;

                case OPERATION_KEY:

                    rco_ScreenState = OPERATION_SCREEN;

                    break;

                case ADJUST_KEY:

                    rco_ScreenState = ADJUST_SCREEN;
                    rco_AdjustscreenState = ADJUST_SCREEN;

                    break;

                case CONFIG_KEY:

                    rco_ScreenState = CONFIG_SCREEN;
                    rco_ConfigscreenState = CONFIG_SCREEN;

                    break;
                }
                display_keyValue = 0xFF;
            }
        }
        else
        {
            rco_ConfignextscreenState = rco_ConfigscreenState;
            rco_ConfigscreenState = CONFIG_PASSWORD_NEGATION_SCREEN;
        }
        break;

    case CONFIG_IDIOME_SCREEN:

        display_loadScreen(DISPLAY_CONFIG_IDIOME_SCREEN);

        control_advanceAuto = 0;

        if (display_Touch())
        {
            display_iconFlag = 1;
            switch (display_keyValue)
            {

            case 0x30:

                display_Language = 0;

                break;

            case 0x31:

                display_Language = 2;

                break;

            case 0x32:

                display_Language = 1;

                break;

            case 0x33:

                flash_updateStatus();
                display_Buzzer(30);
                rco_ScreenState = CONFIG_SCREEN;
                rco_ConfigscreenState = CONFIG_SCREEN;

                break;

            case 0x34:

                display_Language = display_languagePrevious;
                rco_ScreenState = CONFIG_SCREEN;
                rco_ConfigscreenState = CONFIG_SCREEN;

                break;

            case OPERATION_KEY:

                rco_ScreenState = OPERATION_SCREEN;

                break;

            case ADJUST_KEY:

                rco_ScreenState = ADJUST_SCREEN;
                rco_AdjustscreenState = ADJUST_SCREEN;

                break;

            case CONFIG_KEY:

                rco_ScreenState = CONFIG_SCREEN;
                rco_ConfigscreenState = CONFIG_SCREEN;

                break;
            }
        }
        display_keyValue = 0xFF;
        break;

    case CONFIG_DATA_SCREEN:

        display_loadScreen(DISPLAY_CONFIG_DATA_SCREEN);
        control_advanceAuto = 0;
        if (display_Touch())
        {

            display_iconFlag = 1;

            switch (display_keyValue)
            {

            case 0x01:

                display_hourRtc++;
                if (display_hourRtc > 23)
                {
                    display_hourRtc = 0;
                }

                display_fieldUpdate(1, display_hourRtc);

                break;

            case 0x02:

                display_hourRtc--;
                if (display_hourRtc > 23)
                {
                    display_hourRtc = 23;
                }

                display_fieldUpdate(1, display_hourRtc);
                break;

            case 0x03:

                display_minRtc++;
                if (display_minRtc > 59)
                {
                    display_minRtc = 0;
                }

                display_fieldUpdate(2, display_minRtc);
                break;

            case 0x04:

                display_minRtc--;
                if (display_minRtc > 59)
                {
                    display_minRtc = 59;
                }

                display_fieldUpdate(2, display_minRtc);
                break;

            case 0x05:

                display_secRtc++;
                if (display_secRtc > 59)
                {
                    display_secRtc = 0;
                }

                display_fieldUpdate(3, display_secRtc);
                break;

            case 0x06:

                display_secRtc--;
                if (display_secRtc > 59)
                {
                    display_secRtc = 59;
                }

                display_fieldUpdate(3, display_secRtc);

                break;

            case 0x07:

                display_dayRtc++;
                if (display_dayRtc > 31)
                {
                    display_dayRtc = 1;
                }

                display_fieldUpdate(4, display_dayRtc);

                break;

            case 0x08:

                display_dayRtc--;
                if (display_dayRtc > 31)
                {
                    display_dayRtc = 1;
                }

                display_fieldUpdate(4, display_dayRtc);

                break;

            case 0x09:

                display_monthRtc++;
                if (display_monthRtc > 12)
                {
                    display_monthRtc = 1;
                }

                display_fieldUpdate(5, display_monthRtc);

                break;

            case 0x0A:

                display_monthRtc--;
                if (display_monthRtc > 12)
                {
                    display_monthRtc = 1;
                }

                display_fieldUpdate(5, display_monthRtc);

                break;

            case 0x0B:

                display_yearRtc++;
                display_fieldUpdate(6, display_yearRtc);
                break;

            case 0x0C:

                display_yearRtc--;
                display_fieldUpdate(6, display_yearRtc);

                break;

            case 0x0D:

                display_weekRtc++;
                if (display_weekRtc > 7)
                {
                    display_weekRtc = 0;
                }

                display_fieldUpdate(7, display_weekRtc);

                break;

            case 0x0E:

                display_weekRtc--;
                if (display_weekRtc < 0)
                {
                    display_weekRtc = 7;
                }

                display_fieldUpdate(7, display_weekRtc);

                break;

            case 0x10:
                display_rtcWrite();
                display_Buzzer(30);
                rco_ScreenState = CONFIG_SCREEN;
                rco_ConfigscreenState = CONFIG_SCREEN;

                break;

            case 0x11:

                rco_ScreenState = CONFIG_SCREEN;
                rco_ConfigscreenState = CONFIG_SCREEN;

                break;

            case OPERATION_KEY:

                rco_ScreenState = OPERATION_SCREEN;
                rco_AdjustscreenState = ADJUST_SCREEN;
                rco_ConfigscreenState = CONFIG_SCREEN;

                break;

            case ADJUST_KEY:

                rco_ScreenState = ADJUST_SCREEN;
                rco_AdjustscreenState = ADJUST_SCREEN;
                rco_ConfigscreenState = CONFIG_SCREEN;

                break;

            case CONFIG_KEY:

                rco_ScreenState = CONFIG_SCREEN;
                rco_AdjustscreenState = ADJUST_SCREEN;
                rco_ConfigscreenState = CONFIG_SCREEN;

                break;
            }
            display_keyValue = 0xFF;
        }
        break;

    case CONFIG_CALIBRATION_SCREEN:

        display_loadScreen(DISPLAY_CONFIG_CAL_SCREEN);

        if (display_Touch())
        {

            display_iconFlag = 1;

            switch (display_keyValue)
            {

            case BtPosChMais:

                if (rco_ChCalPos > 0)
                {
                    rco_ChCalPos--;
                }

                break;

            case BtPosChMenos:

                if (rco_ChCalPos < 4)
                {
                    rco_ChCalPos++;
                }

                break;

            case BtPos120R:

                operation_standardH(rco_ChCalPos);
                flash_updateStatus();
                break;

            case BtPos96R:

                operation_Coefficient(rco_ChCalPos, 1);
                flash_updateStatus();
                break;

            case 0x35:

                if (adc_Offset[rco_ChCalPos] - 48 > -9)
                {
                    adc_Offset[rco_ChCalPos] = adc_Offset[rco_ChCalPos] - 1;
                }
                if (adc_Offset[rco_ChCalPos] - 48 <= -9)
                {
                    adc_Offset[rco_ChCalPos] = 39;
                }
                flash_updateStatus();
                break;

            case 0x34:

                if (adc_Offset[rco_ChCalPos] - 48 < 9)
                {
                    adc_Offset[rco_ChCalPos] = adc_Offset[rco_ChCalPos] + 1;
                }
                if (adc_Offset[rco_ChCalPos] - 48 >= 9)
                {
                    adc_Offset[rco_ChCalPos] = 57;
                }
                flash_updateStatus();
                break;

            case 0x36:

                if (control_HistTemp[rco_ChCalPos] < 255)
                {
                    control_HistTemp[rco_ChCalPos]--;
                }
                if (control_HistTemp[rco_ChCalPos] >= 255)
                {
                    control_HistTemp[rco_ChCalPos] = 0;
                }
                flash_updateStatus();
                break;

            case 0x37:

                if (control_HistTemp[rco_ChCalPos] < 10)
                {
                    control_HistTemp[rco_ChCalPos]++;
                }
                if (control_HistTemp[rco_ChCalPos] >= 10)
                {
                    control_HistTemp[rco_ChCalPos] = 9;
                }
                flash_updateStatus();

                break;

            case OPERATION_KEY:

                rco_ScreenState = OPERATION_SCREEN;
                rco_ConfigscreenState = CONFIG_SCREEN;

                break;

            case ADJUST_KEY:

                rco_ScreenState = ADJUST_SCREEN;
                rco_AdjustscreenState = ADJUST_SCREEN;

                break;

            case CONFIG_KEY:

                rco_ScreenState = CONFIG_SCREEN;
                rco_ConfigscreenState = CONFIG_SCREEN;

                break;
            }
            display_keyValue = 0xFF;
        }

        break;

    case CONFIG_TESTS_SCREEN:

        display_loadScreen(DISPLAY_CONFIG_TESTS_SCREEN);

        if (display_Touch())
        {

            display_iconFlag = 1;

            switch (display_keyValue)
            {

            case BtTCompressor:

                control_StOutCompressor ^= 0x01;

                break;

            case BtTPeltier:

                control_StOutPeltier ^= 0x01;

                break;

            case BtTUV:

                control_StOutUV ^= 0x01;

                break;

            case BtTInterna:

                control_StOutInterna ^= 0x01;

                break;

            case BtTOzonio:

                control_StOutOzonio ^= 0x01;

                break;

            case BtTFog:

                control_StOutFog ^= 0x01;

                break;

            case BtTExtra:

                control_StOutAdicional ^= 0x01;

                break;

            case BtTSentido1:

                flagDirTeste ^= 1;

                break;

            case BtTSentido2:

                flagPulTeste = 0xFFFF;

                break;

            case BtTHabilitar:

                flagEnaTeste ^= 1;

                break;

            case OPERATION_KEY:

                rco_ScreenState = OPERATION_SCREEN;
                rco_ConfigscreenState = CONFIG_SCREEN;

                break;

            case ADJUST_KEY:

                rco_ScreenState = ADJUST_SCREEN;
                rco_AdjustscreenState = ADJUST_SCREEN;
                rco_ConfigscreenState = CONFIG_SCREEN;

                break;

            case CONFIG_KEY:

                rco_ScreenState = CONFIG_SCREEN;
                rco_ConfigscreenState = CONFIG_SCREEN;

                break;
            }
            display_keyValue = 0xFF;

            if (flagEnaTeste)
            {
                setEna;
            }
            else
            {
                clrEna;
            }
            if (flagDirTeste)
            {
                setDir;
            }
            else
            {
                clrDir;
            }
            while (flagPulTeste)
            {
                flagPulTeste--;
                setPulso;
            }
        }

        break;

    case CONFIG_FACTORY_SCREEN:
        break;

    case CONFIG_PASSWORD_SCREEN:

        display_loadScreen(DISPLAY_CONFIG_PASSWORD_SCREEN);
        control_advanceAuto = 0;

        if (display_Touch())
        {

            display_iconFlag = 1;

            if (rco_contDigitoSenha < 4)
            {

                switch (display_keyValue)
                {
                case 0x00:

                    rco_passwordBuffer[rco_contDigitoSenha] = 0x00;
                    display_iconUpdate((0xE0 + rco_contDigitoSenha), 0x02);
                    rco_contDigitoSenha++;

                    break;

                case 0x01:

                    rco_passwordBuffer[rco_contDigitoSenha] = 0x01;
                    display_iconUpdate((0xE0 + rco_contDigitoSenha), 0x02);
                    rco_contDigitoSenha++;

                    break;

                case 0x02:

                    rco_passwordBuffer[rco_contDigitoSenha] = 0x02;
                    display_iconUpdate((0xE0 + rco_contDigitoSenha), 0x02);
                    rco_contDigitoSenha++;

                    break;

                case 0x03:

                    rco_passwordBuffer[rco_contDigitoSenha] = 0x03;
                    display_iconUpdate((0xE0 + rco_contDigitoSenha), 0x02);
                    rco_contDigitoSenha++;

                    break;

                case 0x04:

                    rco_passwordBuffer[rco_contDigitoSenha] = 0x04;
                    display_iconUpdate((0xE0 + rco_contDigitoSenha), 0x02);
                    rco_contDigitoSenha++;

                    break;

                case 0x05:

                    rco_passwordBuffer[rco_contDigitoSenha] = 0x05;
                    display_iconUpdate((0xE0 + rco_contDigitoSenha), 0x02);
                    rco_contDigitoSenha++;

                    break;

                case 0x06:

                    rco_passwordBuffer[rco_contDigitoSenha] = 0x06;
                    display_iconUpdate((0xE0 + rco_contDigitoSenha), 0x02);
                    rco_contDigitoSenha++;

                    break;

                case 0x07:

                    rco_passwordBuffer[rco_contDigitoSenha] = 0x07;
                    display_iconUpdate((0xE0 + rco_contDigitoSenha), 0x02);
                    rco_contDigitoSenha++;

                    break;

                case 0x08:

                    rco_passwordBuffer[rco_contDigitoSenha] = 0x08;
                    display_iconUpdate((0xE0 + rco_contDigitoSenha), 0x02);
                    rco_contDigitoSenha++;

                    break;

                case 0x09:

                    rco_passwordBuffer[rco_contDigitoSenha] = 0x09;
                    display_iconUpdate((0xE0 + rco_contDigitoSenha), 0x02);
                    rco_contDigitoSenha++;

                    break;

                case 0x3B:

                    if (rco_contDigitoSenha > 0)
                    {
                        rco_contDigitoSenha--;
                        rco_passwordBuffer[rco_contDigitoSenha] = 0xFF;
                        display_iconUpdate((0xE0 + rco_contDigitoSenha), 0x00);
                    }

                    break;

                case OPERATION_KEY:

                    rco_ScreenState = OPERATION_SCREEN;

                    break;

                case ADJUST_KEY:

                    rco_ScreenState = ADJUST_SCREEN;
                    rco_AdjustscreenState = ADJUST_SCREEN;

                    break;

                case CONFIG_KEY:

                    rco_ScreenState = CONFIG_SCREEN;
                    rco_ConfigscreenState = CONFIG_SCREEN;

                    break;
                }
            }
            else
            {
                switch (display_keyValue)
                {
                case 0x3C:
                    rco_passwordSave();
                    rco_passwordClear();
                    rco_ConfigscreenState = CONFIG_PASSWORD_NEW_SCREEN;

                    break;

                case 0x3B:
                    if (rco_contDigitoSenha > 0)
                    {
                        rco_contDigitoSenha--;
                        rco_passwordBuffer[rco_contDigitoSenha] = 0xFF;
                        display_iconUpdate((0xE0 + rco_contDigitoSenha), 0x00);
                    }
                    break;

                case OPERATION_KEY:

                    rco_ScreenState = OPERATION_SCREEN;

                    break;

                case ADJUST_KEY:

                    rco_ScreenState = ADJUST_SCREEN;
                    rco_AdjustscreenState = ADJUST_SCREEN;

                    break;

                case CONFIG_KEY:

                    rco_ScreenState = CONFIG_SCREEN;
                    rco_ConfigscreenState = CONFIG_SCREEN;

                    break;
                }
            }

            display_keyValue = 0xFF;
        }

        break;

    case CONFIG_PASSWORD_NEW_SCREEN:

        control_advanceAuto = 0;

        if (display_Touch())
        {

            display_iconFlag = 1;

            if (rco_contDigitoSenha < 4)
            {
                switch (display_keyValue)
                {
                case 0x00:

                    rco_passwordBuffer[rco_contDigitoSenha] = 0x00;
                    display_iconUpdate((0xE4 + rco_contDigitoSenha), 0x02);
                    rco_contDigitoSenha++;

                    break;

                case 0x01:

                    rco_passwordBuffer[rco_contDigitoSenha] = 0x01;
                    display_iconUpdate((0xE4 + rco_contDigitoSenha), 0x02);
                    rco_contDigitoSenha++;

                    break;

                case 0x02:

                    rco_passwordBuffer[rco_contDigitoSenha] = 0x02;
                    display_iconUpdate((0xE4 + rco_contDigitoSenha), 0x02);
                    rco_contDigitoSenha++;

                    break;

                case 0x03:

                    rco_passwordBuffer[rco_contDigitoSenha] = 0x03;
                    display_iconUpdate((0xE4 + rco_contDigitoSenha), 0x02);
                    rco_contDigitoSenha++;

                    break;

                case 0x04:

                    rco_passwordBuffer[rco_contDigitoSenha] = 0x04;
                    display_iconUpdate((0xE4 + rco_contDigitoSenha), 0x02);
                    rco_contDigitoSenha++;

                    break;

                case 0x05:

                    rco_passwordBuffer[rco_contDigitoSenha] = 0x05;
                    display_iconUpdate((0xE4 + rco_contDigitoSenha), 0x02);
                    rco_contDigitoSenha++;

                    break;

                case 0x06:

                    rco_passwordBuffer[rco_contDigitoSenha] = 0x06;
                    display_iconUpdate((0xE4 + rco_contDigitoSenha), 0x02);
                    rco_contDigitoSenha++;

                    break;

                case 0x07:

                    rco_passwordBuffer[rco_contDigitoSenha] = 0x07;
                    display_iconUpdate((0xE4 + rco_contDigitoSenha), 0x02);
                    rco_contDigitoSenha++;

                    break;

                case 0x08:

                    rco_passwordBuffer[rco_contDigitoSenha] = 0x08;
                    display_iconUpdate((0xE4 + rco_contDigitoSenha), 0x02);
                    rco_contDigitoSenha++;

                    break;

                case 0x09:

                    rco_passwordBuffer[rco_contDigitoSenha] = 0x09;
                    display_iconUpdate((0xE4 + rco_contDigitoSenha), 0x02);
                    rco_contDigitoSenha++;

                    break;

                case 0x3B:

                    if (rco_contDigitoSenha > 0)
                    {
                        rco_contDigitoSenha--;
                        rco_passwordBuffer[rco_contDigitoSenha] = 0xFF;
                        display_iconUpdate((0xE4 + rco_contDigitoSenha), 0x00);
                    }

                    break;

                case OPERATION_KEY:

                    rco_ScreenState = OPERATION_SCREEN;

                    break;

                case ADJUST_KEY:

                    rco_ScreenState = ADJUST_SCREEN;
                    rco_AdjustscreenState = ADJUST_SCREEN;

                    break;

                case CONFIG_KEY:

                    rco_ScreenState = CONFIG_SCREEN;
                    rco_ConfigscreenState = CONFIG_SCREEN;

                    break;
                }
            }
            else
            {
                switch (display_keyValue)
                {

                case 0x3B:

                    if (rco_contDigitoSenha > 0)
                    {
                        rco_contDigitoSenha--;
                        rco_passwordBuffer[rco_contDigitoSenha] = 0xFF;
                        display_iconUpdate((0xE0 + rco_contDigitoSenha), 0x00);
                    }

                    break;

                case 0x3C:

                    if (rco_passwordCompare())
                    {
                        display_loadPopup(POPSENHAINV);
                    }
                    else
                    {
                        display_Buzzer(30);
                        flash_updateStatus();
                    }
                    rco_passwordClear();
                    rco_ConfigscreenState = CONFIG_SCREEN;

                    break;
                }
            }
            display_keyValue = 0xFF;
        }

        break;

    case CONFIG_ABOUT_SCREEN:

        display_loadScreen(DISPLAY_CONFIG_ABOUT_SCREEN);
        control_advanceAuto = 0;

        if (display_Touch())
        {

            display_iconFlag = 1;

            switch (display_keyValue)
            {
            case OPERATION_KEY:

                rco_ScreenState = OPERATION_SCREEN;

                break;

            case ADJUST_KEY:

                rco_ScreenState = ADJUST_SCREEN;
                rco_AdjustscreenState = ADJUST_SCREEN;

                break;

            case CONFIG_KEY:

                rco_ScreenState = CONFIG_SCREEN;
                rco_ConfigscreenState = CONFIG_SCREEN;

                break;
            }
            display_keyValue = 0xFF;
        }

        break;

    case CONFIG_AUTO_ADJUST_SCREEN:

        display_loadScreen(DISPLAY_CONFIG_AUTO_ADJUST_SCREEN);
        control_advanceAuto = 0;

        if (display_Touch())
        {

            display_iconFlag = 1;

            switch (display_keyValue)
            {

            case 0x30:

                break;

            case 0x31:

                break;

            case 0x32:

                operation_peltierDefrostDown();

                break;

            case 0x33:

                operation_peltierDefrostUp();

                break;

            case 0x34:

                operation_chamberDefrostDown();

                break;

            case 0x35:

                operation_chamberDefrostUp();

                break;

            case 0x36:

                break;

            case 0x37:

                break;

            case 0x38:

                operation_peltierStandDown();

                break;

            case 0x39:

                operation_peltierStandUp();

                break;

            case 0x3A:

                operation_chamberStandDown();

                break;

            case 0x3B:

                operation_chamberStandUp();

                break;

            case 0x42:

                control_AutotempFlag = 1;

                flash_updateDefrost();

                break;

            case 0x43:

                control_AutotempFlag = 1;

                flash_updateStand();

                break;

            case OPERATION_KEY:

                if (control_AutotempFlag)
                {
                    control_TdefrostCamaraN = operation_TdefrostCamaraN;
                    control_TdefrostPeltierN = operation_TdefrostPeltierN;
                    control_TstandCamaraN = operation_TstandCamaraN;
                    control_TstandPeltierN = operation_TstandPeltierN;
                }
                else
                {
                    operation_TdefrostCamaraN = control_TdefrostCamaraN;
                    operation_TdefrostPeltierN = control_TdefrostPeltierN;
                    operation_TstandCamaraN = control_TstandCamaraN;
                    operation_TstandPeltierN = control_TstandPeltierN;
                }

                rco_ScreenState = OPERATION_SCREEN;
                rco_AdjustscreenState = ADJUST_SCREEN;
                rco_ConfigscreenState = CONFIG_SCREEN;

                break;

            case ADJUST_KEY:

                if (control_AutotempFlag)
                {
                    control_TdefrostCamaraN = operation_TdefrostCamaraN;
                    control_TdefrostPeltierN = operation_TdefrostPeltierN;
                    control_TstandCamaraN = operation_TstandCamaraN;
                    control_TstandPeltierN = operation_TstandPeltierN;
                }
                else
                {
                    operation_TdefrostCamaraN = control_TdefrostCamaraN;
                    operation_TdefrostPeltierN = control_TdefrostPeltierN;
                    operation_TstandCamaraN = control_TstandCamaraN;
                    operation_TstandPeltierN = control_TstandPeltierN;
                }

                rco_ScreenState = ADJUST_SCREEN;
                rco_AdjustscreenState = ADJUST_SCREEN;
                rco_ConfigscreenState = CONFIG_SCREEN;

                break;

            case CONFIG_KEY:

                if (control_AutotempFlag)
                {
                    control_TdefrostCamaraN = operation_TdefrostCamaraN;
                    control_TdefrostPeltierN = operation_TdefrostPeltierN;
                    control_TstandCamaraN = operation_TstandCamaraN;
                    control_TstandPeltierN = operation_TstandPeltierN;
                }
                else
                {
                    operation_TdefrostCamaraN = control_TdefrostCamaraN;
                    operation_TdefrostPeltierN = control_TdefrostPeltierN;
                    operation_TstandCamaraN = control_TstandCamaraN;
                    operation_TstandPeltierN = control_TstandPeltierN;
                }

                rco_ScreenState = CONFIG_SCREEN;
                rco_ConfigscreenState = CONFIG_SCREEN;

                break;
            }
            display_keyValue = 0xFF;
        }
        break;

    case CONFIG_PASSWORD_NEGATION_SCREEN:

        display_loadScreen(DISPLAY_PASSWORD_CONFIG_SCREEN);

        control_advanceAuto = 0;

        if (display_Touch())
        {

            display_iconFlag = 1;

            if (rco_contDigitoSenha < 4)
            {
                switch (display_keyValue)
                {
                case 0x00:

                    rco_passwordBuffer[rco_contDigitoSenha] = 0x00;
                    display_iconUpdate((0xE0 + rco_contDigitoSenha), 0x02);
                    rco_contDigitoSenha++;

                    break;

                case 0x01:

                    rco_passwordBuffer[rco_contDigitoSenha] = 0x01;
                    display_iconUpdate((0xE0 + rco_contDigitoSenha), 0x02);
                    rco_contDigitoSenha++;

                    break;

                case 0x02:

                    rco_passwordBuffer[rco_contDigitoSenha] = 0x02;
                    display_iconUpdate((0xE0 + rco_contDigitoSenha), 0x02);
                    rco_contDigitoSenha++;

                    break;

                case 0x03:

                    rco_passwordBuffer[rco_contDigitoSenha] = 0x03;
                    display_iconUpdate((0xE0 + rco_contDigitoSenha), 0x02);
                    rco_contDigitoSenha++;

                    break;

                case 0x04:

                    rco_passwordBuffer[rco_contDigitoSenha] = 0x04;
                    display_iconUpdate((0xE0 + rco_contDigitoSenha), 0x02);
                    rco_contDigitoSenha++;

                    break;

                case 0x05:

                    rco_passwordBuffer[rco_contDigitoSenha] = 0x05;
                    display_iconUpdate((0xE0 + rco_contDigitoSenha), 0x02);
                    rco_contDigitoSenha++;

                    break;

                case 0x06:

                    rco_passwordBuffer[rco_contDigitoSenha] = 0x06;
                    display_iconUpdate((0xE0 + rco_contDigitoSenha), 0x02);
                    rco_contDigitoSenha++;

                    break;

                case 0x07:

                    rco_passwordBuffer[rco_contDigitoSenha] = 0x07;
                    display_iconUpdate((0xE0 + rco_contDigitoSenha), 0x02);
                    rco_contDigitoSenha++;

                    break;

                case 0x08:

                    rco_passwordBuffer[rco_contDigitoSenha] = 0x08;
                    display_iconUpdate((0xE0 + rco_contDigitoSenha), 0x02);
                    rco_contDigitoSenha++;

                    break;

                case 0x09:

                    rco_passwordBuffer[rco_contDigitoSenha] = 0x09;
                    display_iconUpdate((0xE0 + rco_contDigitoSenha), 0x02);
                    rco_contDigitoSenha++;

                    break;

                case 0x3B:

                    if (rco_contDigitoSenha > 0)
                    {
                        rco_contDigitoSenha--;
                        rco_passwordBuffer[rco_contDigitoSenha] = 0xFF;
                        display_iconUpdate((0xE0 + rco_contDigitoSenha), 0x00);
                    }

                    break;

                case OPERATION_KEY:

                    rco_ScreenState = OPERATION_SCREEN;

                    break;

                case ADJUST_KEY:

                    rco_ScreenState = ADJUST_SCREEN;
                    rco_AdjustscreenState = ADJUST_SCREEN;

                    break;

                case CONFIG_KEY:

                    rco_ScreenState = CONFIG_SCREEN;
                    rco_ConfigscreenState = CONFIG_SCREEN;

                    break;
                }
                display_keyValue = 0xFF;
            }
            else
            {
                if (1)
                {
                    if (!rco_passwordCheck())
                    {
                        rco_ScreenState = CONFIG_SCREEN;
                        rco_ConfigscreenState = CONFIG_SCREEN;
                        rco_passwordState = 0xAB;
                    }
                    else
                    {
                        display_Buzzer(30);
                        display_loadPopup(POPSENHAINV);
                        rco_ScreenState = CONFIG_SCREEN;
                        rco_ConfigscreenState = CONFIG_PASSWORD_NEGATION_SCREEN;
                    }
                    rco_passwordClear();
                }
            }
        }

        break;
    }
}

char rco_passwordCheck(void)
{
    unsigned char i;
    unsigned char Check;
    Check = 0;

    //checa se digitou senha do usuario
    for (i = 0; rco_contDigitoSenha > i; i++)
    {
        if (flash_userPassword[i] != rco_passwordBuffer[i])
        {
            Check++;
        }
    }
    if (Check)
    {
        Check = 0;

        //checa se digitou senha padrao
        for (i = 0; rco_contDigitoSenha > i; i++)
        {
            if (flash_defaultPassword[i] != rco_passwordBuffer[i])
            {
                Check++;
            }
        }
    }

    return Check;
}

void rco_passwordClear(void)
{
    unsigned char i;
    display_iconUpdate(0xE0, 0x00);
    display_iconUpdate(0xE1, 0x00);
    display_iconUpdate(0xE2, 0x00);
    display_iconUpdate(0xE3, 0x00);

    display_iconUpdate(0xE4, 0x00);
    display_iconUpdate(0xE5, 0x00);
    display_iconUpdate(0xE6, 0x00);
    display_iconUpdate(0xE7, 0x00);

    for (i = 0; rco_contDigitoSenha > i; i++)
    {
        rco_passwordBuffer[i] = 0x00;
    }
    rco_contDigitoSenha = 0;
}

void rco_passwordSave(void)
{
    unsigned char i;
    for (i = 0; rco_contDigitoSenha > i; i++)
    {
        rco_passwordNew[i] = rco_passwordBuffer[i];
    }
}

unsigned char rco_passwordCompare(void)
{
    unsigned char i;
    unsigned char Comparation;
    Comparation = 0;

    for (i = 0; rco_contDigitoSenha > i; i++)
    {
        if (rco_passwordNew[i] != rco_passwordBuffer[i])
        {
            Comparation++;
        }
    } //checa se digitou senha do usuario

    if (!Comparation)
    {
        for (i = 0; rco_contDigitoSenha > i; i++)
        {
            flash_userPassword[i] = rco_passwordNew[i];
        }
    }

    return Comparation;
}