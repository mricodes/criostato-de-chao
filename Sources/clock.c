/*===============================================================================================
/	  EQUIPAMENTO: Placa LPCPU02
/	  EMPRESA: LUPETEC
/   Rotinas para o Clock do microcontrolador
===============================================================================================*/

#include "clock.h"

void clock_Init(void)
{

  //Set watchdog and stop mode configuration
    SOPT = SOPT_CONFIG;

    //Low voltage options configuration
    SPMSC1 = SPMSC1_CONFIG;

    //Low voltage warning function and stop mode configuration
    SPMSC2 = SPMSC2_CONFIG;

    //MCLK control configuration
    SMCLK &= (unsigned char)~0x17;

    //Oscillator and clock mode configuration
    ICGC1 = ICGC1_CONFIG;

    //ICGC2 configuration
    ICGC2 = ICGC2_CONFIG;
    
    if (*(unsigned char*far)0xFFBE != 0xFF) { /* Test if the device trim value is stored on the specified address */
    ICGTRM = *(unsigned char*far)0xFFBE; /* Initialize ICGTRM register from a non volatile memory */
  }
  
    //Wait stabilize of ICG module
    while(!ICGS1_LOCK){}
}