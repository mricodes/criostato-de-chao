/*===============================================================================================
/	EQUIPAMENTO: Placa LPCPU02
/	EMPRESA: LUPETEC
/ Rotinas para o temporizador
===============================================================================================*/

//Includes
#include "timer.h"
#include "systemkernel.h"

void timer1_Init(void)
{

    //Reset register
    TPM1SC = 0x00;

    //Set 5ms interruption
    TPM1MODL = 0x2D;
    TPM1MODH = 0x00;

    (void)(TPM1SC == 0);

    //Configure interruption and prescale registers
    TPM1SC = 0x48;
}

void timer2_Init(void)
{

    //Reset register
    TPM2SC = 0x00;

    //Set 20ms interruption
    TPM2MODL = 0x00;
    TPM2MODH = 0x20;

    (void)(TPM2SC == 0);

    //Configure interruption and prescale registers
    TPM2SC = 0x57;

    //Flag init
    timer2_Flag = STATE_TIMER_START;
}

char timer2_Delay(char Time)
{
    if (timer2_Flag == STATE_TIMER_START)
    {
        timer_Delay = 0;
        timer2_Flag = STATE_TIMER_WAIT;
    }
    else
    {
        if (timer_Delay >= Time)
        {
            if (timer2_Flag == STATE_TIMER_FINISHED)
            {
                timer_Delay = 0;
            }
            timer2_Flag = STATE_TIMER_FINISHED;
            return 1;
        }
    }
    return 0;
}

void timer1_motorControl(void)
{

    if (timer_periodoPulso != 0xFF)
    {
        if (timer_periodoPulsoCont < timer_periodoPulso)
        {
            timer_periodoPulsoCont++;
        }
        else
        {
            trocaPulso;
            timer_periodoPulsoCont = 0;
            if (timer_numPulso - 1 > timer_numPulsoCont)
            {
                timer_numPulsoCont++;
            }
            else
            {
                timer_numPulsoCont = 0;
                if (timer_numPulsoMult > 1)
                {
                    timer_numPulsoMult--;
                }
                else
                {
                    timer_periodoPulso = 0xFF;
                }
            }
        }
    }
    else
    {
        clrPulso;
    }
}