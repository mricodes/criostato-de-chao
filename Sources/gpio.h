/*===============================================================================================
/	EQUIPAMENTO: Placa LPCPU02
/	EMPRESA: LUPETEC
/   Cabecalho dos pinos de entrada e saída
===============================================================================================*/
//Includes 
#include "MC9S08AC32.h"

//GPIO configuration defines
#define GPIO_PORTA 0b11111111
#define GPIO_PORTB 0b00000111
#define GPIO_PORTC 0b11011111
#define GPIO_PORTD 0b11100000
#define GPIO_PORTE 0b11111101
#define GPIO_PORTF 0b00000000
#define GPIO_PORTG 0b00000000


#define TecRetroRapido PTFD_PTFD0
#define TecAvancoRapido PTFD_PTFD1
#define TecAvancoLento PTFD_PTFD2
#define TecRetroLento PTFD_PTFD3

#define TecLateral (TecAvancoLento || TecAvancoRapido || TecRetroLento || TecRetroRapido)

//GPIO functions
void gpio_Init(void);