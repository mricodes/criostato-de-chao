/*===============================================================================================
/	  EQUIPAMENTO: Placa LPCPU02
/	  EMPRESA: LUPETEC
/   Cabecalho de memoria flash
===============================================================================================*/

//Includes
#include <MC9S08AC32.h>
#include "doonstack.h"

//Defines
#define vFCDIV 76

//Functions
void flash_Init(void);
unsigned char flash_Read(unsigned int Adress);
unsigned char flash_Write(unsigned int Adress, unsigned char Data, unsigned char flag_FirstByte);
unsigned char flash_Erase(unsigned int Adress);
unsigned char flash_Update(void);
unsigned char flash_updateStatus(void);
void flash_updateDefrost(void);
void flash_updateStand(void);
void flash_updateNormalTemp(void);


//Variables
extern unsigned char flash_Error;
extern unsigned char flash_defaultPassword[4];
extern unsigned char flash_userPassword[4]; 

extern unsigned int flash_initialSum;
extern unsigned char flash_chinitialSum;
extern unsigned char flash_chcheckSum;

extern unsigned char flash_buffer[10];
extern unsigned char flash_Data;
extern unsigned char flash_Adress;
extern unsigned char flash_saveCoefficient;
extern unsigned char flash_delUpdate;
extern unsigned char flash_flagUpdate;
extern unsigned char flash_resetFlag;