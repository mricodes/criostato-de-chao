/*===============================================================================================
/	  EQUIPAMENTO: Placa LPCPU02
/	  EMPRESA: LUPETEC
/   Funcao principal
===============================================================================================*/

#include "systemkernel.h"
#include <hidef.h>

#ifdef __cplusplus
extern "C"
#endif

    void
    main(void)
{

  //Initialize low level peripherals
  systemkernel_Init();

  //Start low level peripherals
  systemkernel_Start();

  while (1)
  {
    if (systemkernel_flagLoop)
    {
      //Kernel loop functions
      systemkernel_Loop();
      
      //Clear system kernel loop flag
      systemkernel_flagLoop = 0;
    }
  }
}
