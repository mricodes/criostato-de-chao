/*===============================================================================================
/	  EQUIPAMENTO: Placa LPCPU02
/	  EMPRESA: LUPETEC
/   Rotinas para o Clock do microcontrolador
===============================================================================================*/

#include "systemkernel.h"
#include "MC9S08AC32.h"
#include "MCUinit.h"

__interrupt void timer1_Loop(void)
{
    //Kernel flag loop
    systemkernel_flagLoop = 1;

    if (rco_ScreenState != WAIT_SCREEN)
    {
        timer1_motorControl();
    }

    //Clear interrupt flag
    TPM1SC_TOF = TOF_RESET;
}

__interrupt void timer2_Loop(void)
{

    //Kernel looping function
    if (timer2_Flag == STATE_TIMER_START || timer2_Flag == STATE_TIMER_WAIT)
    {
        timer_Delay++;
    }

    if (rco_TimeSetCam > 0)
    {
        rco_TimeSetCam--;
    }

    display_rtcFlag = 1;
    control_flagControlaTemp = 1;
    control_flagControlaEstAuto = 1;
    control_flagMonitorRtc++;
    plan_flagDelta++;

    //One minute Control
    timer_Minutes++;

    if (timer_Minutes >= 116)
    {
        timer_Minutes = 0;
        if (rco_autoState != STATE_AUTO_STAND)
        {
            switch (plan_Timedelta)
            {

            case 1:

                control_deltaDefrost--;

                break;

            case 2:

                control_deltaOzonio--;

                break;

            case 3:

                control_deltaUv--;

                break;
            }
        }
    }

    //Clear interrupt flag
    TPM2SC_TOF = TOF_RESET;
}

__interrupt void adc1_Int(void)
{

    if (cntMediaADC < MEDIA_ADC)
    {
        cntMediaADC++;
        somaADC += (((ADC1RH & 0x03) * 256) + ADC1RL);
        //somaADC += ADCR;
        //adc_Value =  (((ADC1RH & 0x03) << 8) + ADC1RL);
    }
    else
    {

        adc_Value[indexADC] = somaADC / MEDIA_ADC;
        somaADC = 0;
        cntMediaADC = 0;

        if (indexADC < 5)
        {
            indexADC++;
            adc_auxConfig++;
        }
        else
        {
            indexADC = 0;
            adc_auxConfig = 0x43;
        }
        (void)(ADC1RH == 0); // Dummy read
        (void)(ADC1RL == 0);
    }
    ADC1SC1 = adc_auxConfig;
}

__interrupt void uart1_rxI(void)
{

    char uart1_dataReceived;
    (void)SCI1S1;
    (void)SCI1C3;
    uart1_dataReceived = SCI1D;

    //Increase data input position
    uart1_inData++;

    //Check if data input is greater than buffer RX
    if (uart1_inData >= UART_BUFFER_RX)
    {
        uart1_inData = 0;
    }

    //Allocate data input to UART1 RX buffer
    uart1_rxBuffer[uart1_inData] = uart1_dataReceived;
}

__interrupt void uart2_rxI(void)
{
    char uart2_dataReceived;

    uart2_dataReceived = SCI2S1;
    uart2_dataReceived = SCI2C3;
    uart2_dataReceived = SCI2D;

    uart2_inData++;
    if (uart2_inData >= UART_BUFFER_RX)
    {
        uart2_inData = 0;
    }
    uart2_rxBuffer[uart2_inData] = uart2_dataReceived;
}

/* Initialization of the CPU registers in FLASH */

/* NVPROT: FPS7=1,FPS6=1,FPS5=1,FPS4=1,FPS3=1,FPS2=1,FPS1=1,FPDIS=1 */
const unsigned char NVPROT_INIT @0x0000FFBD = 0xFF;

/* NVOPT: KEYEN=0,FNORED=1,SEC01=1,SEC00=0 */
const unsigned char NVOPT_INIT @0x0000FFBF = 0x7E;

extern near void _Startup(void);

/* Interrupt vector table */
#ifndef UNASSIGNED_ISR
#define UNASSIGNED_ISR ((void (*near const)(void))0xFFFF) /* unassigned interrupt service routine */
#endif

void (*near const _vect[])(void) @0xFFC6 = {
    /* Interrupt vector table */
    UNASSIGNED_ISR, /* Int.no. 28 Vtpm3ovf (at FFC6)              Unassigned */
    UNASSIGNED_ISR, /* Int.no. 27 Vtpm3ch1 (at FFC8)              Unassigned */
    UNASSIGNED_ISR, /* Int.no. 26 Vtpm3ch0 (at FFCA)              Unassigned */
    UNASSIGNED_ISR, /* Int.no. 25 Vrti (at FFCC)                  Unassigned */
    UNASSIGNED_ISR, /* Int.no. 24 Viic1 (at FFCE)                 Unassigned */
    adc1_Int,       /* Int.no. 23 Vadc1 (at FFD0)                 Unassigned */
    UNASSIGNED_ISR, /* Int.no. 22 Vkeyboard1 (at FFD2)            Unassigned */
    UNASSIGNED_ISR, /* Int.no. 21 Vsci2tx (at FFD4)               Unassigned */
    uart2_rxI,      /* Int.no. 20 Vsci2rx (at FFD6)               Unassigned */
    UNASSIGNED_ISR, /* Int.no. 19 Vsci2err (at FFD8)              Unassigned */
    UNASSIGNED_ISR, /* Int.no. 18 Vsci1tx (at FFDA)               Unassigned */
    uart1_rxI,      /* Int.no. 17 Vsci1rx (at FFDC)               Unassigned */
    UNASSIGNED_ISR, /* Int.no. 16 Vsci1err (at FFDE)              Unassigned */
    UNASSIGNED_ISR, /* Int.no. 15 Vspi1 (at FFE0)                 Unassigned */
    timer2_Loop,    /* Int.no. 14 Vtpm2ovf (at FFE2)              Unassigned */
    UNASSIGNED_ISR, /* Int.no. 13 Vtpm2ch1 (at FFE4)              Unassigned */
    UNASSIGNED_ISR, /* Int.no. 12 Vtpm2ch0 (at FFE6)              Unassigned */
    timer1_Loop,    /* Int.no. 11 Vtpm1ovf (at FFE8)              Unassigned */
    UNASSIGNED_ISR, /* Int.no. 10 Vtpm1ch5 (at FFEA)              Unassigned */
    UNASSIGNED_ISR, /* Int.no.  9 Vtpm1ch4 (at FFEC)              Unassigned */
    UNASSIGNED_ISR, /* Int.no.  8 Vtpm1ch3 (at FFEE)              Unassigned */
    UNASSIGNED_ISR, /* Int.no.  7 Vtpm1ch2 (at FFF0)              Unassigned */
    UNASSIGNED_ISR, /* Int.no.  6 Vtpm1ch1 (at FFF2)              Unassigned */
    UNASSIGNED_ISR, /* Int.no.  5 Vtpm1ch0 (at FFF4)              Unassigned */
    isrVicg,        /* Int.no.  4 Vicg (at FFF6)                  Used */
    UNASSIGNED_ISR, /* Int.no.  3 Vlvd (at FFF8)                  Unassigned */
    UNASSIGNED_ISR, /* Int.no.  2 Virq (at FFFA)                  Unassigned */
    UNASSIGNED_ISR, /* Int.no.  1 Vswi (at FFFC)                  Unassigned */
    _Startup        /* Int.no.  0 Vreset (at FFFE)                Reset vector */
};

__interrupt void isrVicg(void)
{
    /* Write your interrupt code here ... */
    char aux;
    aux = ICGS1;
    ICGS1_ICGIF = 1;
    /*  System clock initialization */
    /* ICGC1: HGO=0,RANGE=1,REFS=1,CLKS1=1,CLKS0=1,OSCSTEN=1,LOCD=0 */
    ICGC1 = 0x7C;
    /* ICGC2: LOLRE=0,MFD2=0,MFD1=0,MFD0=0,LOCRE=0,RFD2=0,RFD1=0,RFD0=0 */
    ICGC2 = 0x00;
    if (*(unsigned char *far)0xFFBE != 0xFF)
    {                                         /* Test if the device trim value is stored on the specified address */
        ICGTRM = *(unsigned char *far)0xFFBE; /* Initialize ICGTRM register from a non volatile memory */
    }
    while (!ICGS1_LOCK)
    { /* Wait */
    }
}

/* END MCUinit */

/*
** ###################################################################
**
**     This file was created by Processor Expert 3.07 [04.34]
**     for the Freescale HCS08 series of microcontrollers.
**
** ###################################################################
*/
