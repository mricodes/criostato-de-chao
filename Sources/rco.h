/*===============================================================================================
/	  EQUIPAMENTO: Placa LPCPU02
/	  EMPRESA: LUPETEC
/   Cabecalho da comunicacao com operador
===============================================================================================*/

//Includes
#include "MC9S08AC32.h"


//Defines
//Screen
#define DISPLAY_OPERATION_MAIN_SCREEN      0x01
#define DISPLAY_ADJUST_MAIN_SCREEN         0x04
#define DISPLAY_CONFIG_MAIN_SCREEN         0x07
#define DISPLAY_CONFIG_CAL_SCREEN       13
#define DISPLAY_CONFIG_DATA_SCREEN      16
#define DISPLAY_CONFIG_IDIOME_SCREEN    19
#define DISPLAY_PROG_PRESET_SCREEN    22
#define DISPLAY_PASSWORD_CONFIG_SCREEN     28
#define DISPLAY_CONFIG_TESTS_SCREEN    31
#define DISPLAY_CONFIG_PASSWORD_SCREEN     34
#define DISPLAY_CONFIG_ABOUT_SCREEN     25
#define DISPLAY_CONFIG_AUTO_ADJUST_SCREEN 37
#define DISPLAY_ADJUST_PROG_AUTO_SCREEN 10

//RCO state machine defines
#define WAIT_SCREEN 0
#define OPERATION_SCREEN 1
#define ADJUST_SCREEN 2
#define ADJUST_PROG_AUTO_SCREEN 3
#define ADJUST_PRESET_SCREEN 4
#define CONFIG_SCREEN 5
#define CONFIG_IDIOME_SCREEN 6
#define CONFIG_DATA_SCREEN 7
#define CONFIG_CALIBRATION_SCREEN 8
#define CONFIG_TESTS_SCREEN 9
#define CONFIG_FACTORY_SCREEN 10
#define CONFIG_PASSWORD_SCREEN 11
#define CONFIG_PASSWORD_NEW_SCREEN 12
#define CONFIG_ABOUT_SCREEN 13
#define CONFIG_AUTO_ADJUST_SCREEN 14
#define CONFIG_PASSWORD_NEGATION_SCREEN 15

//State Variables
extern unsigned char rco_blockedState;
extern unsigned char rco_ScreenState;
extern unsigned char rco_AdjustscreenState;
extern unsigned char rco_ConfigscreenState;
extern unsigned char rco_StTrim;
extern unsigned char rco_autoState;
extern unsigned char rco_tempautoState;
extern unsigned char rco_lastautoState;
extern unsigned char rco_progUpdate;

//Variables
extern unsigned char rco_numpulseAdjustment;
extern unsigned char rco_periodAdjustment;
extern int rco_trimValue[7];
extern int rco_sectionValue[7];
extern unsigned char rco_TimeSetCam;
extern int rco_totalCut;
extern unsigned int rco_totalMicras;
extern unsigned char rco_ChCalPos;
extern unsigned char rco_ChCalNeg;

//Temperature Variables
extern unsigned char rco_fTsetCamara[3];
extern unsigned char rco_fTsetPeltier[3];
extern unsigned char rco_fTsetAmostra[3];
extern unsigned char rco_fTsetExtra[3];

extern unsigned char rco_passwordState;
extern unsigned char rco_ConfignextscreenState;
extern unsigned char rco_passwordBuffer[4];
extern unsigned char rco_passwordNew[4];
extern unsigned char rco_contDigitoSenha;

void rco_Loop(void);
void rco_operationState(void);
void rco_adjustState(void);
void rco_configState(void);
char rco_passwordCheck(void);
void rco_passwordClear(void);
void rco_passwordSave(void);
unsigned char rco_passwordCompare(void);