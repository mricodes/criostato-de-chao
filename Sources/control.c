/*===============================================================================================
/	    EQUIPAMENTO: Placa LPCPU02
/	    EMPRESA: LUPETEC
/     Funcao de comunicao com operador
===============================================================================================*/

#include "control.h"
#include "systemkernel.h"

void control_Loop(void)
{

    if (control_advanceAuto == 2)
    {
        control_automaticAd();
    }

    if (control_flagControlaTemp)
    {

        control_temperature();
        control_flagControlaTemp = 0;
    }
    if (control_flagMonitorRtc >= 10)
    {

        control_monitorRtc();
        control_flagMonitorRtc = 0;
    }

    if (control_flagControlaEstAuto)
    {

        control_automaticState();
        control_flagControlaEstAuto = 0;
    }

    control_Reles();
}

char control_motorAdvance()
{
    if (fimCurso1)
    {

        return 0;
    }
    else
    {
        if (control_Micras > 0)
        {
            clrDir;
            clrEna;
            timer_numPulso = control_Micras;
            timer_numPulsoMult = rco_numpulseAdjustment;
            timer_periodoPulso = rco_periodAdjustment;
        }

        return 1;
    }
}

char control_motorRetreat()
{
    if (fimCurso1)
    {

        return 0;
    }
    else
    {

        setDir;
        clrEna;
        timer_numPulso = control_Micras;
        timer_numPulsoMult = rco_numpulseAdjustment;
        timer_periodoPulso = rco_periodAdjustment;

        return 1;
    }
}

char control_motortotalRetreat()
{
    if (!fimCurso1)
    {
        while (control_motorRetreat())
        {
            rco_periodAdjustment = 1;
            rco_numpulseAdjustment = 48;
            control_Micras = 250;
        }
        return 1;
    }
    else
    {
        return 0;
    }
}

void control_automaticAd(void)
{
    if (opticoCima && control_unicoAvanco && !opticoBaixo)
    {
        control_unicoAvanco = 0;
        control_unicoRetorno = 1;
        rco_numpulseAdjustment = 10;
        rco_periodAdjustment = 5;
        control_Micras = control_Micras + (control_RetracValue * 10);

        if (!control_motorAdvance())
        {
            display_loadPopup(POPFIMCURSO);
        }

        control_Micras = control_Micras - (control_RetracValue * 10);

        if ((rco_totalMicras + control_Micras) < 33000)
        {
            rco_totalMicras += control_Micras;
        }

        if (rco_totalCut < 2500)
        {
            rco_totalCut++;
        }
    }

    if (opticoBaixo && control_unicoRetorno)
    {

        control_unicoAvanco = 1;

        if (!fimCurso1)
        {
            setDir;
            clrEna;
            timer_numPulso = control_RetracValue * 10;
            timer_numPulsoMult = rco_numpulseAdjustment;
            timer_periodoPulso = rco_periodAdjustment;
        }

        control_unicoRetorno = 0;
    }
}

void control_Reles(void)
{
    if (control_StOutInterna)
    {
        OutInterna = 0x01;
    }
    else
    {
        OutInterna = 0x00;
    }

    if (control_StOutUV)
    {
        OutUV = 0x01;
    }
    else
    {
        OutUV = 0x00;
    }

    if (control_StOutCompressor)
    {
        OutCompressor = 0x01;
    }
    else
    {
        OutCompressor = 0x00;
    }

    if (control_StOutOzonio)
    {
        OutOzonio = 0x01;
    }
    else
    {
        OutOzonio = 0x00;
    }

    if (control_StOutPeltier)
    {
        OutPeltier = 0x01;
    }
    else
    {
        OutPeltier = 0x00;
    }

    if (control_StOutFog)
    {
        OutFog = 0x01;
    }
    else
    {
        OutFog = 0x00;
    }

    if (control_StOutAdicional)
    {
        OutAdicional = 0x01;
    }
    else
    {
        OutAdicional = 0x00;
    }
}

void control_temperature(void)
{

    control_TemperatureRead(TEMPERATURE_AMBIENT);
    if (adc_TOut < 9999)
        control_TAmbiente = adc_TOut;

    control_TemperatureRead(TEMPERATURE_CHAMBER);
    if (adc_TOut < 9999)
        control_TCamara = adc_TOut;

    control_TemperatureRead(TEMPERATURE_PELTIER);
    if (adc_TOut < 9999)
        control_TPeltier = adc_TOut;

    control_TemperatureRead(TEMPERATURE_SAMPLE);
    if (adc_TOut < 9999)
        control_TAmostra = adc_TOut;

    control_TemperatureRead(TEMPERATURE_EXTRA);
    if (adc_TOut < 9999)
        control_TExtra = adc_TOut;

    if (rco_ConfigscreenState != CONFIG_TESTS_SCREEN && rco_ScreenState > 0)
    {

        //Chamber control compressor operation
        control_ErroTemp = control_TCamara - control_TSetCamara;
        if (control_ErroTemp + control_HistTemp[TEMPERATURE_CHAMBER] > 0)
        {
            control_StOutCompressor = 1;
        }
        else if (control_ErroTemp - control_HistTemp[TEMPERATURE_CHAMBER] < 0)
        {
            control_StOutCompressor = 0;
        }

        //Peltier control operation
        control_ErroTemp = control_TPeltier - control_TSetPeltier;
        if (control_ErroTemp + control_HistTemp[TEMPERATURE_PELTIER] > 0)
        {
            control_StOutPeltier = 1;
        }
        else if (control_ErroTemp - control_HistTemp[TEMPERATURE_PELTIER] < 0)
        {
            control_StOutPeltier = 0;
        }
    }
}

void control_TemperatureRead(unsigned char Channel)
{
    adc_Taux = adc_Value[Channel];
    adc_Taux1 = adc_Taux * 1000;
    adc_Taux = adc_Taux1 / A_CH[Channel];
    adc_Taux1 = adc_Taux - B_CH[Channel];
    adc_TOut = adc_Taux1 - adc_Offset[Channel];
}

void control_automaticState(void)
{

    if (rco_ConfigscreenState != CONFIG_DATA_SCREEN)
    {
        unsigned char index1 = 0;
        unsigned char NBH = 0;
        unsigned char NBL = 0;

        NBH = display_weekRtc;

        //Defrost
        NBL = 0;
        index1 = NBH * 5 + NBL;

        if (plan_hourProg[index1] == display_hourRtc && plan_minProg[index1] == display_minRtc)
        {
            if (rco_autoState != STATE_AUTO_DEFROST)
            {
                rco_autoState = STATE_AUTO_DEFROST;
                control_StartprogHour = display_hourRtc;
                control_StartprogMin = display_minRtc;
                display_iconFlag = 1;
                timer_Minutes = 0;
            }
        }

        //Ozonio
        NBL = 1;
        index1 = NBH * 5 + NBL;

        if (plan_Timedelta == 0 && rco_autoState == STATE_AUTO_DEFROST)
        {
            plan_Timedelta = 1;
            if (plan_hourProg[index1] - control_StartprogHour < 0)
            {
                control_deltaDefrost += (24 + plan_hourProg[index1] - control_StartprogHour) * 60;
            }
            else
            {
                control_deltaDefrost += (plan_hourProg[index1] - control_StartprogHour) * 60;
            }

            control_deltaDefrost += (plan_minProg[index1] - control_StartprogMin);
        }

        //ozonio state set
        if ((plan_Timedelta == 1 && control_deltaDefrost == 0) || (plan_hourProg[index1] == display_hourRtc && plan_minProg[index1] == display_minRtc))
        {
            if (rco_autoState != STATE_AUTO_OZONIO)
            {
                control_deltaDefrost = 0;
                rco_autoState = STATE_AUTO_OZONIO;
                control_StartprogHour = display_hourRtc;
                control_StartprogMin = display_minRtc;
                display_iconFlag = 1;
                timer_Minutes = 0;
            }
        }

        //UV
        NBL = 2;
        index1 = NBH * 5 + NBL;

        if (rco_autoState == STATE_AUTO_OZONIO && plan_Timedelta != 2)
        {
            plan_Timedelta = 2;
            if (plan_hourProg[index1] - control_StartprogHour < 0)
            {
                control_deltaOzonio += (24 + plan_hourProg[index1] - control_StartprogHour) * 60;
            }
            else
            {
                control_deltaOzonio += (plan_hourProg[index1] - control_StartprogHour) * 60;
            }

            control_deltaOzonio += (plan_minProg[index1] - control_StartprogMin);
        }

        if ((control_deltaOzonio == 0 && plan_Timedelta == 2) || (plan_hourProg[index1] == display_hourRtc && plan_minProg[index1] == display_minRtc))
        {
            if (rco_autoState != STATE_AUTO_UV)
            {
                control_deltaOzonio = 0;
                rco_autoState = STATE_AUTO_UV;
                control_StartprogHour = display_hourRtc;
                control_StartprogMin = display_minRtc;
                display_iconFlag = 1;
                timer_Minutes = 0;
            }
        }

        //Stand-By
        NBL = 3;
        index1 = NBH * 5 + NBL;

        if (rco_autoState == STATE_AUTO_UV && plan_Timedelta != 3)
        {
            plan_Timedelta = 3;
            if (plan_hourProg[index1] - control_StartprogHour < 0)
            {
                control_deltaUv += (24 + plan_hourProg[index1] - control_StartprogHour) * 60;
            }
            else
            {
                control_deltaUv += (plan_hourProg[index1] - control_StartprogHour) * 60;
            }

            control_deltaUv += (plan_minProg[index1] - control_StartprogMin);
        }

        if ((control_deltaUv == 0 && plan_Timedelta == 3) || (plan_hourProg[index1] == display_hourRtc && plan_minProg[index1] == display_minRtc))
        {
            if (rco_autoState != STATE_AUTO_STAND)
            {
                control_deltaUv = 0;
                rco_autoState = STATE_AUTO_STAND;
                display_iconFlag = 1;
                timer_Minutes = 0;
            }
        }

        //Wake
        NBL = 4;

        index1 = NBH * 5 + NBL;

        if (display_hourRtc == plan_hourProg[index1] && display_minRtc == plan_minProg[index1])
        {
            if (rco_autoState != STATE_AUTO_NORMAL)
            {
                rco_autoState = STATE_AUTO_NORMAL;
                plan_Timedelta = 0;
                control_StartprogHour = display_hourRtc;
                control_StartprogMin = display_minRtc;
                display_iconFlag = 1;
                timer_Minutes = 0;
            }
        }
    }

    if (rco_ConfigscreenState != CONFIG_AUTO_ADJUST_SCREEN)
    {

        switch (rco_autoState)
        {

        case STATE_AUTO_NORMAL:

            if (control_temperatureFlag)
            {
                control_TSetCamara = control_TSetCamaraN;
                control_TSetPeltier = control_TSetPeltierN;
            }

            break;

        case STATE_AUTO_STAND:

            if (control_AutotempFlag)
            {
                control_TSetCamara = control_TstandCamaraN;
                control_TSetPeltier = control_TstandPeltierN;
                control_StOutUV = 0x00;
                control_StOutOzonio = 0x00;
                control_StOutAdicional = 0x00;
                display_iconFlag = 1;
            }

            break;

        case STATE_AUTO_DEFROST:

            if (control_AutotempFlag)
            {
                control_TSetCamara = control_TdefrostCamaraN;
                control_TSetPeltier = control_TdefrostPeltierN;
                control_StOutUV = 0x00;
                control_StOutOzonio = 0x00;
                control_StOutAdicional = 0x01;
                display_iconFlag = 1;
            }

            break;

        case STATE_AUTO_OZONIO:

            if (control_AutotempFlag)
            {
                control_TSetCamara = control_TdefrostCamaraN;
                control_TSetPeltier = control_TdefrostPeltierN;
                control_StOutUV = 0x00;
                control_StOutOzonio = 0x01;
                control_StOutAdicional = 0x00;
                display_iconFlag = 1;
            }

            break;

        case STATE_AUTO_UV:

            if (control_AutotempFlag)
            {
                control_TSetCamara = control_TdefrostCamaraN;
                control_TSetPeltier = control_TdefrostPeltierN;
                control_StOutUV = 0x01;
                control_StOutOzonio = 0x00;
                control_StOutAdicional = 0x00;
                display_iconFlag = 1;
            }

            break;

        case STATE_AUTO_RETRO:

            display_loadPopup(POPRETROMOTOR);
            if (!control_motortotalRetreat())
            {
                display_loadPopup(0x0D);
                display_Buzzer(30);
                rco_autoState = rco_lastautoState;
            }
            else
            {
                display_loadPopup(0x0D);
                display_Buzzer(30);
                rco_autoState = rco_lastautoState;
            }

            break;
        }
    }
}

void control_monitorRtc(void)
{
    if (rco_AdjustscreenState != ADJUST_PROG_AUTO_SCREEN && rco_ConfigscreenState != CONFIG_DATA_SCREEN)
    {
        display_rtcRead();
    }
}