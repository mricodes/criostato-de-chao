/*===============================================================================================
/	EQUIPAMENTO: Placa LPCPU02
/	EMPRESA: LUPETEC
/   Cabecalho do Nucleo do microcontrolador
===============================================================================================*/

//Includes Low level
#include "clock.h"
#include "gpio.h"
#include "timer.h"
#include "uart.h"
#include "doonstack.h"
#include "conversion.h"
#include "adc.h"

//Includes "Medium" level
#include "display.h"
#include "plan.h"
#include "flash.h"

//Operation includes
#include "operation.h"
#include "rco.h"
#include "control.h"

//System kernel init
void systemkernel_Init(void);

//System kernel start
void systemkernel_Start(void);

//System kernel Looping
void systemkernel_Loop(void);

//Variables (find function to put it)
extern unsigned char A_1[5];
extern unsigned char A_2[5];
extern unsigned char A_3[5];
extern unsigned char B_1[5];
extern unsigned char B_2[5];
extern unsigned char B_3[5];

extern long int A_CH[5];
extern long int B_CH[5];
extern unsigned char systemkernel_flagLoop;
