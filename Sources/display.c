/*===============================================================================================
/	EQUIPAMENTO: Placa LPCPU02
/	EMPRESA: LUPETEC
/   Rotinas para o display
===============================================================================================*/

//Includes
#include "display.h"
#include "systemkernel.h"

//Variables
int display_State = WAIT_SCREEN;

void display_Loop(void)
{
    //Icon Screen update
    display_iconState();

    //Values Screen update
    display_valueState();
}

void display_loadScreen(unsigned char Screen)
{

    unsigned char display_auxScreen = 0;
    display_auxScreen = Screen + display_Language;

    if (display_auxScreen != display_currentScreen)
    {
        uart2_Tx(0xA5);
        uart2_Tx(0x5A);
        uart2_Tx(0x04);
        uart2_Tx(0x80);
        uart2_Tx(0x03);
        uart2_Tx(0x00);
        uart2_Tx(display_auxScreen);
        display_currentScreen = display_auxScreen;
    }
}
void display_loadPopup(unsigned char Pop)
{
    unsigned char auxPop = Pop; // + offSetIdioma;
    uart2_Tx(0xA5);
    uart2_Tx(0x5A);
    uart2_Tx(0x03);
    uart2_Tx(0x80);
    uart2_Tx(0x4F);
    uart2_Tx(auxPop);
}

void display_Buzzer(unsigned char Time)
{

    uart2_Tx(0xA5);
    uart2_Tx(0x5A);
    uart2_Tx(0x03);
    uart2_Tx(0x80);
    uart2_Tx(0x02);
    uart2_Tx(Time);
}

char display_Touch(void)
{
    char Feedback = 0;
    if (display_rxreadValue[2] != 0xFF)
    {
        display_keyValue = display_rxreadValue[2];
        display_rxreadValue[2] = 0xFF;
        display_keyAdress = display_rxadressLow;

        if (rco_blockedState != STATE_AUTO_BLOCKED)
        {
            Feedback = 1;
        }
        else
        {
            if (display_keyValue == OPERATION_KEY_BLOCK)
            {
                Feedback = 1;
            }
            else
            {
                display_keyValue = 0xFF;
                Feedback = 0;
            }
        }
    }
    else
    {
        display_keyValue = 0XFF;
        Feedback = 0;
    }
    return Feedback;
}

void display_textUpdate(unsigned char Position, unsigned char Length)
{
    unsigned char i;

    //Compose display message
    uart2_Tx(0xA5);
    uart2_Tx(0x5A);
    uart2_Tx(Length + 0x03);
    uart2_Tx(0x82);
    uart2_Tx(0x00);
    uart2_Tx(Position);

    for (i = 0; i < Length; i++)
    {
        uart2_Tx(display_textBuffer[i]);
    }
}

void display_fieldUpdate(unsigned char Position, unsigned char Value)
{
    uart2_Tx(0xA5);
    uart2_Tx(0x5A);
    uart2_Tx(0x05);
    uart2_Tx(0x82);
    uart2_Tx(0x00);
    uart2_Tx(Position);
    uart2_Tx(Value / 256);
    uart2_Tx(Value);
}

void display_iconUpdate(unsigned char Position, unsigned char Value)
{
    uart2_Tx(0xA5);
    uart2_Tx(0x5A);
    uart2_Tx(0x05);
    uart2_Tx(0x82);
    uart2_Tx(0x01);
    uart2_Tx(Position);
    uart2_Tx(0x00);
    uart2_Tx(Value);
}

void display_rtcRead(void)
{
    uart2_Tx(0xA5);
    uart2_Tx(0x5A);
    uart2_Tx(0x03);
    uart2_Tx(0x81);
    uart2_Tx(0x20);
    uart2_Tx(0x07);
    uart2_Tx(0x03);

    display_rxState = 0;
    uart2_inData = 0;
    uart2_outData = 0;
    display_rxreadValue[6] = 0xFF;

    while (display_rxreadValue[6] > 0xFE)
    {
        if (display_rtcFlag == 1)
        {
            display_rxreadValue[6] = 0xFF;
            display_rtcFlag = 0;
        }
        display_rxProtocol();
    }

    display_yearRtc = conversion_HextoDec(display_rxreadValue[0]);
    display_monthRtc = conversion_HextoDec(display_rxreadValue[1]);
    display_dayRtc = conversion_HextoDec(display_rxreadValue[2]);
    display_weekRtc = conversion_HextoDec(display_rxreadValue[3]);
    display_hourRtc = conversion_HextoDec(display_rxreadValue[4]);
    display_minRtc = conversion_HextoDec(display_rxreadValue[5]);
    display_secRtc = conversion_HextoDec(display_rxreadValue[6]);

    display_rxreadValue[0] = 0xFF;
    display_rxreadValue[1] = 0xFF;
    display_rxreadValue[2] = 0xFF;
    display_rxreadValue[3] = 0xFF;
    display_rxreadValue[4] = 0xFF;
    display_rxreadValue[5] = 0xFF;
    display_rxreadValue[6] = 0xFF;

    display_rxState = 0;
    uart2_inData = 0;
    uart2_outData = 0;

    if (rco_ConfigscreenState == CONFIG_DATA_SCREEN)
    {
        //timer2_Flag = STATE_TIMER_START;

        display_fieldUpdate(6, display_yearRtc);
        display_fieldUpdate(5, display_monthRtc);
        display_fieldUpdate(4, display_dayRtc);
        display_fieldUpdate(1, display_hourRtc);
        display_fieldUpdate(2, display_minRtc);
        display_fieldUpdate(3, display_secRtc);
        display_fieldUpdate(7, display_weekRtc);
    }
}

void display_rtcWrite(void)
{
    uart2_Tx(0xA5);
    uart2_Tx(0x5A);
    uart2_Tx(0x0A);
    uart2_Tx(0x80);
    uart2_Tx(0x1F);
    uart2_Tx(0x5A);
    uart2_Tx(conversion_DectoHex(display_yearRtc));
    uart2_Tx(conversion_DectoHex(display_monthRtc));
    uart2_Tx(conversion_DectoHex(display_dayRtc));
    uart2_Tx(0x00);
    uart2_Tx(conversion_DectoHex(display_hourRtc));
    uart2_Tx(conversion_DectoHex(display_minRtc));
    uart2_Tx(conversion_DectoHex(display_secRtc));
}

void display_decUpdate(char posH, char posL, char nDec)
{
    uart2_Tx(0xA5);
    uart2_Tx(0x5A);
    uart2_Tx(0x05);
    uart2_Tx(0x82);
    uart2_Tx(posH);
    uart2_Tx(posL + 0x06);
    uart2_Tx(nDec);
    uart2_Tx(0x00);
}

void display_temperatureUpdate(unsigned char Type, unsigned char Position)
{
    long int Temperature;
    switch (Type)
    {
    case TEMPERATURE_AMBIENT:

        if (operation_chCalibrado[Type] == 1)
            Temperature = control_TAmbiente;
        else
        {
            display_textBuffer[0] = '-';
            display_textBuffer[1] = '-';
            display_textBuffer[2] = '-';
            display_textBuffer[3] = '-';
            display_textUpdate(Position, 4);
            return;
        }

        break;

    case TEMPERATURE_CHAMBER:

        if (operation_chCalibrado[Type] == 1)
            Temperature = control_TCamara;
        else
        {
            display_textBuffer[0] = '-';
            display_textBuffer[1] = '-';
            display_textBuffer[2] = '-';
            display_textBuffer[3] = '-';
            display_textUpdate(Position, 4);
            return;
        }

        break;

    case TEMPERATURE_PELTIER:

        if (operation_chCalibrado[Type] == 1)
            Temperature = control_TPeltier;
        else
        {
            display_textBuffer[0] = '-';
            display_textBuffer[1] = '-';
            display_textBuffer[2] = '-';
            display_textBuffer[3] = '-';
            display_textUpdate(Position, 4);
            return;
        }

        break;

    case TEMPERATURE_SAMPLE:

        if (operation_chCalibrado[Type] == 1)
            Temperature = control_TAmostra;
        else
        {
            display_textBuffer[0] = '-';
            display_textBuffer[1] = '-';
            display_textBuffer[2] = '-';
            display_textBuffer[3] = '-';
            display_textUpdate(Position, 4);
            return;
        }

        break;

    case TEMPERATURE_EXTRA:

        if (operation_chCalibrado[Type] == 1)
            Temperature = control_TExtra;
        else
        {
            display_textBuffer[0] = '-';
            display_textBuffer[1] = '-';
            display_textBuffer[2] = '-';
            display_textBuffer[3] = '-';
            display_textUpdate(Position, 4);
            return;
        }

        break;
    case TEMPERATURE_SAMPLE_UPDATE:

        Temperature = control_TSetAmostraN;

        break;

    case TEMPERATURE_PELTIER_UPDATE:

        Temperature = control_TSetPeltierN;

        break;
    case OPERATION_TEMPERATURE_PELTIER_UPDATE:

        Temperature = operation_TSetPeltierN;

        break;

    case TEMPERATURE_CHAMBER_UPDATE:

        Temperature = control_TSetCamaraN;

        break;

    case OPERATION_TEMPERATURE_CHAMBER_UPDATE:

        Temperature = operation_TSetCamaraN;

        break;

    case TEMPERATURE_DEFROST_AUTO_PELTIER:

        Temperature = operation_TdefrostPeltierN;

        break;

    case TEMPERATURE_DEFROST_AUTO_CHAMBER:

        Temperature = operation_TdefrostCamaraN;

        break;

    case TEMPERATURE_STAND_AUTO_PELTIER:

        Temperature = operation_TstandPeltierN;

        break;

    case TEMPERATURE_STAND_AUTO_CHAMBER:

        Temperature = operation_TstandCamaraN;

        break;
    case TEMPERATURE_OFFSET:

        Temperature = 48 - adc_Offset[rco_ChCalPos];

        break;
    }

    if (Temperature < 0)
    {
        Temperature = Temperature * (-1);
        conversion_toAscii(Temperature);
        conversion_Msg[0] = '-';
    }
    else
    {
        conversion_toAscii(Temperature);
        conversion_Msg[0] = '+';
    }
    if (Type == TEMPERATURE_OFFSET)
    {
        display_textBuffer[0] = conversion_Msg[0];
        display_textBuffer[1] = ' ';
        display_textBuffer[2] = conversion_Msg[3];
        display_textBuffer[3] = ' ';
    }
    else
    {
        if (Temperature > 99)
        {
            display_textBuffer[0] = conversion_Msg[0];
            display_textBuffer[1] = conversion_Msg[1];
            display_textBuffer[2] = conversion_Msg[2];
            display_textBuffer[3] = conversion_Msg[3];
        }
        else
        {
            display_textBuffer[0] = conversion_Msg[0];
            display_textBuffer[1] = ' ';
            display_textBuffer[2] = conversion_Msg[2];
            display_textBuffer[3] = conversion_Msg[3];
        }
    }

    display_textUpdate(Position, 4);
}

void display_iconState(void)
{
    if (display_iconFlag)
    {
        switch (rco_ScreenState)
        {
        case OPERATION_SCREEN:

            if (control_StOutInterna)
            {
                display_iconUpdate(ICON_INTERNA, 0x01);
            }
            else
            {
                display_iconUpdate(ICON_INTERNA, 0x00);
            }

            if (control_StOutUV)
            {
                display_iconUpdate(ICON_UV, 0x01);
            }
            else
            {
                display_iconUpdate(ICON_UV, 0x00);
            }

            if (control_StOutOzonio)
            {
                display_iconUpdate(ICON_OZONIO, 0x01);
            }
            else
            {
                display_iconUpdate(ICON_OZONIO, 0x00);
            }
            if (rco_StTrim)
            {
                display_iconUpdate(ICON_TRIM, 0x01);
                display_iconUpdate(ICON_SEC, 0x00);
            }
            else
            {
                display_iconUpdate(ICON_TRIM, 0x00);
                display_iconUpdate(ICON_SEC, 0x01);
            }
            display_iconUpdate(0X58, opticoVolante);

            switch (rco_autoState)
            {
            case STATE_AUTO_NORMAL:
                display_iconUpdate(ICON_STAND, 0x00);
                display_iconUpdate(ICON_DEFROST, 0x00);
                break;

            case STATE_AUTO_STAND:
                display_iconUpdate(ICON_STAND, 0x01);
                display_iconUpdate(ICON_DEFROST, 0x00);
                break;

            case STATE_AUTO_DEFROST:
                display_iconUpdate(ICON_STAND, 0x00);
                display_iconUpdate(ICON_DEFROST, 0x01);
                break;

            case STATE_AUTO_UV:
                display_iconUpdate(ICON_STAND, 0x00);
                display_iconUpdate(ICON_DEFROST, 0x00);
                break;

            case STATE_AUTO_OZONIO:
                display_iconUpdate(ICON_STAND, 0x00);
                display_iconUpdate(ICON_DEFROST, 0x00);
                break;
            }
            if (rco_blockedState == STATE_AUTO_BLOCKED)
            {
                display_iconUpdate(ICON_BLOCK, 0x01);
            }
            else
            {
                display_iconUpdate(ICON_BLOCK, 0x00);
            }

            break;

        case ADJUST_SCREEN:

            switch (rco_AdjustscreenState)
            {

            case ADJUST_PROG_AUTO_SCREEN:

                //update Week icon
                display_iconUpdate(0x62, plan_nbH);

                //Update function icon
                display_iconUpdate(0x60, plan_nbL);

                //Clear flag

                break;
            }

            break;

        case CONFIG_SCREEN:

            switch (rco_ConfigscreenState)
            {
            case CONFIG_TESTS_SCREEN:

                //Fim de curso
                display_iconUpdate(0x59, fimCurso1);
                display_iconUpdate(0x60, fimCurso0);
                display_iconUpdate(0x61, fimTampa);

                //Keyboard
                display_iconUpdate(0x62, TecAvancoLento);
                display_iconUpdate(0x63, TecAvancoRapido);
                display_iconUpdate(0x64, TecRetroLento);
                display_iconUpdate(0x65, TecRetroRapido);

                //Optic sensor
                display_iconUpdate(0x66, opticoCima);
                display_iconUpdate(0x67, opticoBaixo);
                display_iconUpdate(0x68, opticoVolante);

                //Clear flag
                //display_iconFlag = 0;

                break;

            case CONFIG_CALIBRATION_SCREEN:
                display_iconUpdate(0xE8, rco_ChCalPos);
                display_iconUpdate(0xE9, operation_chCalibrado[rco_ChCalPos]);

                break;
            }

            break;
        }
    }
}

void display_valueState(void)
{
    unsigned char in;
    unsigned char aux;
    unsigned char i;
    aux = 0;
    switch (rco_ScreenState)
    {
    case OPERATION_SCREEN:
        display_temperatureUpdate(TEMPERATURE_AMBIENT, 0x26);
        display_temperatureUpdate(TEMPERATURE_SAMPLE, 0x2B);
        display_temperatureUpdate(TEMPERATURE_PELTIER, 0x30);
        display_temperatureUpdate(TEMPERATURE_CHAMBER, 0x35);

        display_fieldUpdate(0x01, rco_trimValue[0]);
        display_fieldUpdate(0x06, rco_sectionValue[0]);
        display_fieldUpdate(0x0F, rco_totalCut);
        display_fieldUpdate(0x0C, rco_totalMicras);

        break;

    case ADJUST_SCREEN:

        switch (rco_AdjustscreenState)
        {
        case ADJUST_SCREEN:
            display_temperatureUpdate(TEMPERATURE_SAMPLE_UPDATE, 0x2B);
            display_temperatureUpdate(OPERATION_TEMPERATURE_PELTIER_UPDATE, 0x30);
            display_temperatureUpdate(OPERATION_TEMPERATURE_CHAMBER_UPDATE, 0x35);
            display_fieldUpdate(0x06, control_RetracValue);
            break;

        case ADJUST_PROG_AUTO_SCREEN:

            switch (plan_ProgUpdate)
            {

            case 1:
                for (in = 0; in < 35; in++)
                {

                    if (plan_hourProg[in] == 0xFF)
                    {
                        aux = 4;
                        display_textBuffer[0] = '-';
                        display_textBuffer[1] = '-';
                        display_textBuffer[2] = ':';
                        display_textBuffer[3] = '-';
                        display_textBuffer[4] = '-';
                        for (i = in - aux; i <= in; i++)
                        {

                            display_textUpdate(0x2A + (6 * i), 5);
                        }
                    }
                    else
                    {
                        conversion_toAscii(plan_hourProg[in]);
                        display_textBuffer[0] = conversion_Msg[2];
                        display_textBuffer[1] = conversion_Msg[3];
                        display_textBuffer[2] = ':';
                        conversion_toAscii(plan_minProg[in]);
                        display_textBuffer[3] = conversion_Msg[2];
                        display_textBuffer[4] = conversion_Msg[3];

                        display_textUpdate(0x2A + (6 * in), 5);
                    }
                }

                plan_ProgUpdate = 2;
                break;

            case 2:

                if (plan_hourProgTemp != 0xFF)
                {
                    conversion_toAscii(plan_minProgTemp);
                    display_textBuffer[0] = conversion_Msg[2];
                    display_textBuffer[1] = conversion_Msg[3];
                    display_textUpdate(0x24, 2);
                    conversion_toAscii(plan_hourProgTemp);
                    display_textBuffer[0] = conversion_Msg[2];
                    display_textBuffer[1] = conversion_Msg[3];
                    display_textUpdate(0x1E, 2);
                }
                else
                {
                    display_textBuffer[0] = '-';
                    display_textBuffer[1] = '-';
                    display_textUpdate(0x24, 2);
                    display_textBuffer[0] = '-';
                    display_textBuffer[1] = '-';
                    display_textUpdate(0x1E, 2);
                }
                plan_ProgUpdate = 0;
                break;

            case 0:
                return;
                break;
            }

            break;
        }
        break;

    case CONFIG_SCREEN:
        switch (rco_ConfigscreenState)
        {

        case CONFIG_CALIBRATION_SCREEN:

            display_fieldUpdate(0x01, (5 - rco_ChCalPos));
            display_fieldUpdate(0x03, (5 - rco_ChCalNeg));
            display_fieldUpdate(0x42, control_HistTemp[rco_ChCalPos]);

            display_temperatureUpdate(rco_ChCalPos, 0x26);
            display_temperatureUpdate(rco_ChCalNeg, 0x3C);
            display_temperatureUpdate(TEMPERATURE_OFFSET, 0x29);

            break;

        case CONFIG_TESTS_SCREEN:
            //Temperature ambient
            conversion_toAscii(adc_Value[4]);
            display_textBuffer[0] = conversion_Msg[0];
            display_textBuffer[1] = conversion_Msg[1];
            display_textBuffer[2] = conversion_Msg[2];
            display_textBuffer[3] = conversion_Msg[3];
            display_textUpdate(0x26, 4);

            //Temperature Chamber
            conversion_toAscii(adc_Value[3]);
            display_textBuffer[0] = conversion_Msg[0];
            display_textBuffer[1] = conversion_Msg[1];
            display_textBuffer[2] = conversion_Msg[2];
            display_textBuffer[3] = conversion_Msg[3];
            display_textUpdate(0x2D, 4);

            //Temperature Cabecote
            conversion_toAscii(adc_Value[2]);
            display_textBuffer[0] = conversion_Msg[0];
            display_textBuffer[1] = conversion_Msg[1];
            display_textBuffer[2] = conversion_Msg[2];
            display_textBuffer[3] = conversion_Msg[3];
            display_textUpdate(0x33, 4);

            //Temperature Sample
            conversion_toAscii(adc_Value[1]);
            display_textBuffer[0] = conversion_Msg[0];
            display_textBuffer[1] = conversion_Msg[1];
            display_textBuffer[2] = conversion_Msg[2];
            display_textBuffer[3] = conversion_Msg[3];
            display_textUpdate(0x39, 4);

            //Temperature Extra
            conversion_toAscii(adc_Value[0]);
            display_textBuffer[0] = conversion_Msg[0];
            display_textBuffer[1] = conversion_Msg[1];
            display_textBuffer[2] = conversion_Msg[2];
            display_textBuffer[3] = conversion_Msg[3];
            display_textUpdate(0x3F, 4);

            break;

        case CONFIG_AUTO_ADJUST_SCREEN:

            //Auto Screen Display
            display_temperatureUpdate(TEMPERATURE_DEFROST_AUTO_PELTIER, 0x39);
            display_temperatureUpdate(TEMPERATURE_DEFROST_AUTO_CHAMBER, 0x3B);
            display_temperatureUpdate(TEMPERATURE_STAND_AUTO_PELTIER, 0x3F);
            display_temperatureUpdate(TEMPERATURE_STAND_AUTO_CHAMBER, 0x41);
            display_temperatureUpdate(TEMPERATURE_PELTIER_UPDATE, 0x37);
            display_temperatureUpdate(TEMPERATURE_PELTIER_UPDATE, 0x3D);

            //Temperature sensores
            display_temperatureUpdate(TEMPERATURE_SAMPLE, 0x2B);
            display_temperatureUpdate(TEMPERATURE_PELTIER, 0x30);
            display_temperatureUpdate(TEMPERATURE_CHAMBER, 0x35);
            display_temperatureUpdate(TEMPERATURE_AMBIENT, 0x26);

            break;
        }
        break;
    }
}

void display_rxProtocol(void)
{
    //Estado 0////////////////////////////////////////////////////////////////////////////////////
    if (display_rxState == 0)
    {
        if (uart2_Rx())
        {
            if (uart2_rxBuffer[uart2_outData] == 0xA5)
            {
                display_rxState++;
            }
            else
            {
                uart2_inData = uart2_outData = 0;
            }
        }
    }
    //Estado 1////////////////////////////////////////////////////////////////////////////////////
    else if (display_rxState == 1)
    {
        if (uart2_Rx())
        {
            if (uart2_rxBuffer[uart2_outData] == 0x5A)
            {
                display_rxState++;
            }
            else
            {
                display_rxState = uart2_inData = uart2_outData = 0;
            }
        }
    }
    //Estado 2////////////////////////////////////////////////////////////////////////////////////
    else if (display_rxState == 2)
    {
        if (uart2_Rx())
        {
            display_numBytes = uart2_rxBuffer[uart2_outData];
            if (display_rxState < (display_numBytes + 2))
            {
                display_rxState++;
            }
            else
            {
                display_rxState = 0;
                uart2_inData = uart2_outData = 0;
            }
        }
    }
    //Estado 3////////////////////////////////////////////////////////////////////////////////////
    else if (display_rxState == 3)
    {
        if (uart2_Rx())
        {
            display_rxCommand = uart2_rxBuffer[uart2_outData];
            if (display_rxState < (display_numBytes + 2))
            {
                display_rxState++;
            }
            else
            {
                display_rxState = 0;
                uart2_inData = uart2_outData = 0;
            }
        }
    }
    //Estado 4////////////////////////////////////////////////////////////////////////////////////
    else if (display_rxState == 4)
    {
        if (uart2_Rx())
        {
            display_rxadressHigh = uart2_rxBuffer[uart2_outData];
            if (display_rxState < (display_numBytes + 2))
            {
                display_rxState++;
            }
            else
            {
                display_rxState = 0;
                uart2_inData = uart2_outData = 0;
            }
        }
    }
    //Estado 5////////////////////////////////////////////////////////////////////////////////////
    else if (display_rxState == 5)
    {
        if (uart2_Rx())
        {
            display_rxadressLow = uart2_rxBuffer[uart2_outData];
            if (display_rxState < (display_numBytes + 2))
            {
                display_rxState++;
            }
            else
            {
                display_rxState = 0;
                uart2_inData = uart2_outData = 0;
            }
        }
    }
    //Estado 6////////////////////////////////////////////////////////////////////////////////////
    else if (display_rxState == 6)
    {
        if (uart2_Rx())
        {
            display_rxreadValue[0] = uart2_rxBuffer[uart2_outData];
            if (display_rxState < (display_numBytes + 2))
            {
                display_rxState++;
            }
            else
            {
                display_rxState = 0;
                uart2_inData = uart2_outData = 0;
            }
        }
    }
    //Estado 7////////////////////////////////////////////////////////////////////////////////////
    else if (display_rxState == 7)
    {
        if (uart2_Rx())
        {
            display_rxreadValue[1] = uart2_rxBuffer[uart2_outData];
            if (display_rxState < (display_numBytes + 2))
            {
                display_rxState++;
            }
            else
            {
                display_rxState = 0;
                uart2_inData = uart2_outData = 0;
            }
        }
    }
    //Estado 8////////////////////////////////////////////////////////////////////////////////////
    else if (display_rxState == 8)
    {
        if (uart2_Rx())
        {
            display_rxreadValue[2] = uart2_rxBuffer[uart2_outData];
            if (display_rxState < (display_numBytes + 2))
            {
                display_rxState++;
                //display_rxState = 0;
            }
            else
            {
                display_rxState = 0;
                uart2_inData = uart2_outData = 0;
            }
        }
    }

    //Estado 9////////////////////////////////////////////////////////////////////////////////////
    else if (display_rxState == 9)
    {
        if (uart2_Rx())
        {
            display_rxreadValue[3] = uart2_rxBuffer[uart2_outData];
            if (display_rxState < (display_numBytes + 2))
            {
                display_rxState++;
            }
            else
            {
                display_rxState = 0;
                uart2_inData = uart2_outData = 0;
            }
        }
    }
    //Estado 10////////////////////////////////////////////////////////////////////////////////////
    else if (display_rxState == 10)
    {
        if (uart2_Rx())
        {
            display_rxreadValue[4] = uart2_rxBuffer[uart2_outData];
            if (display_rxState < (display_numBytes + 2))
            {
                display_rxState++;
            }
            else
            {
                display_rxState = 0;
                uart2_inData = uart2_outData = 0;
            }
        }
    }
    //Estado 11////////////////////////////////////////////////////////////////////////////////////
    else if (display_rxState == 11)
    {
        if (uart2_Rx())
        {
            display_rxreadValue[5] = uart2_rxBuffer[uart2_outData];
            if (display_rxState < (display_numBytes + 2))
            {
                display_rxState++;
            }
            else
            {
                display_rxState = 0;
                uart2_inData = uart2_outData = 0;
            }
        }
    }
    //Estado 12////////////////////////////////////////////////////////////////////////////////////
    else if (display_rxState == 12)
    {
        if (uart2_Rx())
        {
            display_rxreadValue[6] = uart2_rxBuffer[uart2_outData];
            if (display_rxState < (display_numBytes + 2))
            {
                //display_rxState++;
                display_rxState = 0;
            }
            else
            {
                display_rxState = 0;
                uart2_inData = uart2_outData = 0;
            }
        }
    }
}

void display_timeReset(void)
{
    display_yearRtc = 0xFF;
    display_monthRtc = 0xFF;
    display_dayRtc = 0xFF;
    display_hourRtc = 0xFF;
    display_minRtc = 0xFF;
    display_secRtc = 0xFF;
    display_weekRtc = 0xFF;
}
