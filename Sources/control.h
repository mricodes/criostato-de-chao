/*===============================================================================================
/	  EQUIPAMENTO: Placa LPCPU02
/	  EMPRESA: LUPETEC
/   Cabecalho da comunicacao com operador
===============================================================================================*/

//Includes
#include "MC9S08AC32.h"

//Defines
#define setDir PTBD |= (1 << 1)
#define clrDir PTBD &= ~(1 << 1)

#define trocaPulso PTBD ^= (1 << 0)
#define clrPulso PTBD &= ~(1 << 0)
#define setPulso PTBD |= (1 << 0)

#define setEna PTBD |= (1 << 2)
#define clrEna PTBD &= ~(1 << 2)

//Macros Acionamentos
#define OutCompressor PTAD_PTAD1
#define OutPeltier PTAD_PTAD2
#define OutUV PTAD_PTAD3
#define OutInterna PTAD_PTAD4
#define OutOzonio PTAD_PTAD5
#define OutFog PTAD_PTAD6
#define OutAdicional PTAD_PTAD7

//Variables
extern unsigned char control_RetracValue;
extern unsigned char control_HistTemp[5];
extern unsigned char control_advanceAuto;
extern unsigned char control_unicoAvanco;
extern unsigned char control_unicoRetorno;

//Temperature variables
extern long int control_Temp;
extern long int control_TSetCamaraN;

extern long int control_TSetAmostraN;
extern long int control_TSetPeltierN;
extern unsigned char control_temperatureFlag;
extern unsigned char control_AutotempFlag;
extern long int control_TSetExtraN;
extern unsigned char control_autoDefrostCamera[3];
extern unsigned char control_autoDefrostPeltier[3];
extern unsigned char control_autoStandCamera[3];
extern unsigned char control_autoStandPeltier[3];
extern long int control_TdefrostCamaraN;
extern long int control_TdefrostPeltierN;
extern long int control_TstandCamaraN;
extern long int control_TstandPeltierN;

////Sets de temperatura efetivos

extern long int control_TSetCamara;
extern long int control_TSetPeltier;
extern long int control_TSetAmostra;
extern long int control_TSetExtra;

//Control states variables
extern unsigned char control_StOutCompressor;
extern unsigned char control_StOutPeltier;
extern unsigned char control_StOutUV;
extern unsigned char control_StOutInterna;
extern unsigned char control_StOutOzonio;

extern unsigned char control_StOutFog;
extern unsigned char control_StOutAdicional;
extern int control_Micras;
extern unsigned char flagEnaTeste;
extern unsigned char flagDirTeste;
extern unsigned int flagPulTeste;
extern unsigned char control_flagControlaTemp;
extern unsigned char control_flagControlaEstAuto;
extern unsigned char control_flagMonitorRtc;

//Plan time variables
extern int control_StartprogHour;
extern int control_StartprogMin;
extern int control_deltaDefrost;
extern int control_deltaOzonio;
extern int control_deltaUv;
extern int control_deltaSleep;
extern int control_deltaWakeup;
extern unsigned char plan_Timedelta;

extern long int control_TAmbiente;
extern long int control_TCamara;
extern long int control_TPeltier;
extern long int control_TAmostra;
extern long int control_TExtra;
extern long int control_ErroTemp;

//Functions
void control_Loop(void);
char control_motorAdvance();
char control_motorRetreat();
char control_motortotalRetreat();
void control_automaticAd(void);
void control_Reles(void);
void control_temperature(void);
void control_TemperatureRead(unsigned char Channel);
void control_automaticState(void);
void control_monitorRtc(void);