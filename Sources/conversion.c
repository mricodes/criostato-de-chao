/*===============================================================================================
/	  EQUIPAMENTO: Placa LPCPU02
/	  EMPRESA: LUPETEC
/   Rotinas de conversao
===============================================================================================*/

//Includes
#include "conversion.h"

unsigned char conversion_HextoDec(unsigned char hexa)
{

    unsigned char A;
    unsigned char B;
    A = hexa / 16;
    B = hexa % 16;
    return A * 10 + B;
}

unsigned char conversion_DectoHex(unsigned char dec)
{

    unsigned char A;
    A = (dec / 10) * 16 + (dec % 10);
    return A;
}

void conversion_toAscii(unsigned int num)
{
    if (num <= 9999)
    {
        conversion_Msg[0] = '0';
        conversion_Msg[1] = '0';
        conversion_Msg[2] = '0';
        conversion_Msg[3] = '0';

        while (num >= 1000)
        {
            num = num - 1000;
            conversion_Msg[0]++;
            
        }
        while (num >= 100)
        {
            num = num - 100;
            conversion_Msg[1]++;
        }
        while (num >= 10)
        {
            num = num - 10;
            conversion_Msg[2]++;
        }
        conversion_Msg[3] = '0' + num;
    }
    else
    {
        conversion_Msg[0] = 'x';
        conversion_Msg[1] = 'x';
        conversion_Msg[2] = 'x';
        conversion_Msg[3] = 'x';
    }
}
