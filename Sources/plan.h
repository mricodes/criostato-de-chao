/*===============================================================================================
/	  EQUIPAMENTO: Placa LPCPU02
/	  EMPRESA: LUPETEC
/   Cabecalho de programacao de funcoes
===============================================================================================*/


//Includes
extern unsigned char plan_hourProg[35];
extern unsigned char plan_minProg[35];
extern unsigned char plan_minProgTemp;
extern unsigned char plan_hourProgTemp;
extern unsigned char plan_nbH;
extern unsigned char plan_nbL;
extern unsigned char plan_ProgUpdate;
extern unsigned char plan_indexProg;
extern unsigned char plan_hourDelta;
extern unsigned char plan_minDelta;
extern unsigned char plan_flagDelta;
//Functions

void plan_hourUp(void);
void plan_hourDown(void);
void plan_minUp(void);
void plan_minDown(void);