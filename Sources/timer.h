/*===============================================================================================
/	EQUIPAMENTO: Placa LPCPU02
/	EMPRESA: LUPETEC
/   Cabecalho para o timer
===============================================================================================*/

//TIMER Includes
#include "MC9S08AC32.h"

//TIMER1 defines
#define TOF_RESET 0
#define MS_20 0xC34FU //TPM2SC = 0x4B;
#define MS_5 0x303DU //TPM1SC = 0x48;
#define STATE_TIMER_WAIT 0x00
#define STATE_TIMER_START 0x01
#define STATE_TIMER_FINISHED 0x02

//TIMER functions
void timer1_Init(void); 
void timer2_Init(void);
char timer2_Delay(char Time);
void timer1_motorControl(void);

//TIMER variables
extern int timer_Delay;
extern unsigned char timer2_Flag;
extern unsigned char timer_numPulso;
extern unsigned char timer_numPulsoCont;
extern unsigned char timer_numPulsoMult;
extern unsigned char timer_periodoPulso;
extern unsigned char timer_periodoPulsoCont;
extern unsigned char timer_Minutes;