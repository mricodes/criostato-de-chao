/*===============================================================================================
/	EQUIPAMENTO: Placa LPCPU02
/	EMPRESA: LUPETEC
/   Cabecalho de comunicacao serial
===============================================================================================*/

//UART Includes
#include "MC9S08AC32.h"

//UART defines
#define UART_BUFFER_RX 40

//UART1 variables
extern unsigned char uart1_inData;
extern unsigned char uart1_outData;
extern unsigned char uart1_rxBuffer[40];

//UART functions
void uart1_Init(void);
void uart2_Init(void);
void uart1_tx(char Data);
void uart2_Tx(char Data);
char uart1_Rx();
char uart2_Rx();

//UART2 variables
extern unsigned char uart2_inData;
extern unsigned char uart2_outData;
extern unsigned char uart2_rxBuffer[40];
extern unsigned char uart2_rxState;