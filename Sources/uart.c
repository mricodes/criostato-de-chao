/*===============================================================================================
/	EQUIPAMENTO: Placa LPCPU02
/	EMPRESA: LUPETEC
/   Rotinas para comunicacao serial
===============================================================================================*/

//Includes
#include "uart.h"
#include "systemkernel.h"

void uart1_Init(void)
{

    SCI1C2 = 0x00;

    (void)(SCI1S1 == 0); 
    (void)(SCI1D == 0);

    SCI1S2 = 0x00;
    SCI1BDH = 0x00;
    SCI1BDL = 0x0B;
    SCI1C1 = 0x00;
    SCI1C3 = 0x00;
    SCI1C2 = 0x2C; 
}

void uart2_Init(void)
{

    SCI2C2 = 0x00;

    (void)(SCI2S1 == 0); 
    (void)(SCI2D == 0);

    SCI2S2 = 0x00;
    SCI2BDH = 0x00;
    SCI2BDL = 0x0B;
    SCI2C1 = 0x00;
    SCI2C3 = 0x00;
    SCI2C2 = 0x2C;
}

void uart1_Tx(char Data)
{

    //While buffer is full
    while (!SCI1S1_TDRE)
    {
    }
    SCI1D = Data;
}

void uart2_Tx(char Data)
{

    //While buffer is full
    while (!SCI2S1_TDRE)
    {
    }
    SCI2D = Data;
}

char uart1_Rx()
{

    //
    if (uart1_outData != uart1_inData)
    {
        uart1_outData++;
        if (uart1_outData >= UART_BUFFER_RX)
        {
            uart1_outData = 0;
        }
        return (1);
    }
    else
    {
        return (0);
    }
}

char uart2_Rx()
{

    //
    if (uart2_outData != uart2_inData)
    {
        uart2_outData++;
        if (uart2_outData >= UART_BUFFER_RX)
        {
            uart2_outData = 0;
        }
        return (1);
    }
    else
    {
        return (0);
    }
}

