/*===============================================================================================
/	EQUIPAMENTO: Placa LPCPU02
/	EMPRESA: LUPETEC
/ Rotinas do nucleo
===============================================================================================*/

#include "systemkernel.h"

int i = 0;

void systemkernel_Init()
{

  //Initialize Clock
  clock_Init();

  //Initialize GPIO
  gpio_Init();

  //Initialize timer 1
  timer1_Init();
  timer2_Init();

  //Initialize UARTS
  uart1_Init();
  uart2_Init();

  //Initialize Flash memory
  //flash_Init();

  //Initialize ADC
  adc_Init();

  //Initialize interrupts
  asm CLI;
}

void systemkernel_Start()
{

  //Serial initiate variables
  uart1_inData = 0;
  uart1_outData = 0;
  uart2_inData = 0;
  uart2_outData = 0;

  //Display serial initiate
  display_rxState = 0;

  rco_autoState = STATE_AUTO_NORMAL;
  rco_ScreenState = WAIT_SCREEN;
  rco_AdjustscreenState = ADJUST_SCREEN;
  rco_ConfigscreenState = CONFIG_SCREEN;

  display_keyValue = 0xFF;
  systemkernel_flagLoop = 0;
  rco_StTrim = 0;
  timer_periodoPulso = 0;

  setEna;

  rco_contDigitoSenha = 0;

  flagEnaTeste = 0;
  flagDirTeste = 0;
  flagPulTeste = 0;
  indexADC = 0;
  cntMediaADC = 0;
  somaADC = 0;
  control_unicoAvanco = 1;
  timer_Minutes = 0;
  plan_Timedelta = 0;
  control_temperatureFlag = 1;
  control_AutotempFlag = 1;
  flash_resetFlag = 0;
  flash_defaultPassword[0] = 0x04;
  flash_defaultPassword[1] = 0x04;
  flash_defaultPassword[2] = 0x04;
  flash_defaultPassword[3] = 0x04;
}

void systemkernel_Loop()
{

  //Display loop routine
  display_Loop();

  //Operation with communicator Loop
  rco_Loop();

  //Received message protocol state machine
  display_rxProtocol();

  //Control loop
  control_Loop();

  //Update temperatura display
  operation_Loop();

  //Flash Update
  flash_Update();
}