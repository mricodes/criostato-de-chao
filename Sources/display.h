/*===============================================================================================
/	EQUIPAMENTO: Placa LPCPU02
/	EMPRESA: LUPETEC
/   Cabecalho para o display
===============================================================================================*/

//DISPLAY Includes 
#include "MC9S08AC32.h"


//DISPLAY variables
extern unsigned char display_Language;
extern unsigned char display_languagePrevious;
extern unsigned char display_currentScreen;
extern unsigned char display_rxreadValue[6];
extern unsigned char display_rxState;
extern unsigned char display_numBytes;
extern unsigned char display_rxadressHigh;
extern unsigned char display_rxadressLow;
extern unsigned char display_rxCommand;
extern unsigned char display_keyValue;
extern unsigned char display_keyAdress;
extern unsigned char display_textBuffer[20];
extern unsigned char display_rtcFlag;

//DISPLAY time variables
extern unsigned char display_yearRtc;
extern unsigned char display_monthRtc;
extern unsigned char display_weekRtc;
extern unsigned char display_dayRtc;
extern unsigned char display_hourRtc;
extern unsigned char display_minRtc;
extern unsigned char display_secRtc;

//Display peripherals variables
extern unsigned char display_iconFlag;

//DISPLAY functions
void display_Loop(void);
void display_loadScreen(unsigned char Screen);
void display_loadPopup(unsigned char Pop);
void display_Buzzer(unsigned char Time);
char display_Touch(void);
void display_textUpdate(unsigned char Position, unsigned char Length);
void display_fieldUpdate(unsigned char Position, unsigned char Value);
void display_iconUpdate(unsigned char Position, unsigned char Value);
void display_rtcRead(void);
void display_rtcWrite(void);
void display_decUpdate(char posH, char posL, char nDec);
void display_temperatureUpdate(unsigned char Type, unsigned char Position);
void display_iconState(void);
void display_valueState(void);
void display_rxProtocol(void);
void display_timeReset(void);
