/*===============================================================================================
/	  EQUIPAMENTO: Placa LPCPU02
/	  EMPRESA: LUPETEC
/   Cabecalho para as conversoes
===============================================================================================*/

//Variables
extern unsigned char conversion_Msg[4];

//Functions
unsigned char conversion_HextoDec(unsigned char hexa);
unsigned char conversion_DectoHex(unsigned char dec);
void conversion_toAscii(unsigned int num);