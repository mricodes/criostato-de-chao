/*
** ###################################################################
**     This code is generated by the Device Initialization Tool.
**     It is overwritten during code generation.
**     USER MODIFICATION ARE PRESERVED ONLY INSIDE INTERRUPT SERVICE ROUTINES
**     OR EXPLICITLY MARKED SECTIONS
**
**     Project   : LPCPU02_V102
**     Processor : MC9S08AC32CFU
**     Version   : Component 01.004, Driver 01.06, CPU db: 3.00.038
**     Datasheet : MC9S08AC60 Rev. 2 3/2008
**     Date/Time : 05/06/2019, 15:15
**     Abstract  :
**         This module contains device initialization code 
**         for selected on-chip peripherals.
**     Contents  :
**         Function "MCU_init" initializes selected peripherals
**
**     Copyright : 1997 - 2009 Freescale Semiconductor, Inc. All Rights Reserved.
**     
**     http      : www.freescale.com
**     mail      : support@freescale.com
** ###################################################################
*/

/* MODULE MCUinit */

#include <MC9S08AC60.h> /* I/O map for MC9S08AC32CFU */
#include "MCUinit.h"

/* User declarations and definitions */
/*   Code, declarations and definitions here will be preserved during code generation */
/* End of user declarations and definitions */

/*
** ===================================================================
**     Method      :  MCU_init (component MC9S08AC60_64)
**
**     Description :
**         Device initialization code for selected peripherals.
** ===================================================================
*/
void MCU_init(void)
{
  /* ### MC9S08AC60_64 "Cpu" init code ... */
  /*  PE initialization code after reset */

  /* Common initialization of the write once registers */
  /* SOPT: COPE=0,COPT=1,STOPE=0 */
  SOPT = 0x53;
  /* SPMSC1: LVDF=0,LVDACK=0,LVDIE=0,LVDRE=1,LVDSE=1,LVDE=1,BGBE=0 */
  SPMSC1 = 0x1C;
  /* SPMSC2: LVWF=0,LVWACK=0,LVDV=0,LVWV=0,PPDF=0,PPDACK=0,PPDC=0 */
  SPMSC2 = 0x00;
  /* SMCLK: MPE=0,MCSEL=0 */
  SMCLK &= (unsigned char)~0x17;
  /*  System clock initialization */
  /* ICGC1: HGO=0,RANGE=1,REFS=0,CLKS1=0,CLKS0=0,OSCSTEN=1,LOCD=0 */
  ICGC1 = 0x44;
  /* ICGFLT: FLT11=0,FLT10=0,FLT9=0,FLT8=0,FLT7=1,FLT6=1,FLT5=0,FLT4=0,FLT3=0,FLT2=0,FLT1=0,FLT0=0 */
  ICGFLT = 0xC0U; /* Initialization of the filter value */
  /* ICGC2: LOLRE=0,MFD2=0,MFD1=0,MFD0=0,LOCRE=0,RFD2=0,RFD1=0,RFD0=0 */
  ICGC2 = 0x00;
  if (*(unsigned char *far)0xFFBE != 0xFF)
  {                                       /* Test if the device trim value is stored on the specified address */
    ICGTRM = *(unsigned char *far)0xFFBE; /* Initialize ICGTRM register from a non volatile memory */
  }
  /* Common initialization of the CPU registers */
  /* PTASE: PTASE7=0,PTASE6=0,PTASE5=0,PTASE4=0,PTASE3=0,PTASE2=0,PTASE1=0,PTASE0=0 */
  PTASE = 0x00;
  /* PTBSE: PTBSE7=0,PTBSE6=0,PTBSE5=0,PTBSE4=0,PTBSE3=0,PTBSE2=0,PTBSE1=0,PTBSE0=0 */
  PTBSE = 0x00;
  /* PTCSE: PTCSE6=0,PTCSE5=0,PTCSE4=0,PTCSE3=0,PTCSE2=0,PTCSE1=0,PTCSE0=0 */
  PTCSE &= (unsigned char)~0x7F;
  /* PTDSE: PTDSE7=0,PTDSE6=0,PTDSE5=0,PTDSE4=0,PTDSE3=0,PTDSE2=0,PTDSE1=0,PTDSE0=0 */
  PTDSE = 0x00;
  /* PTESE: PTESE7=0,PTESE6=0,PTESE5=0,PTESE4=0,PTESE3=0,PTESE2=0,PTESE1=0,PTESE0=0 */
  PTESE = 0x00;
  /* PTFSE: PTFSE7=0,PTFSE6=0,PTFSE5=0,PTFSE4=0,PTFSE3=0,PTFSE2=0,PTFSE1=0,PTFSE0=0 */
  PTFSE = 0x00;
  /* PTGSE: PTGSE6=0,PTGSE5=0,PTGSE4=0,PTGSE3=0,PTGSE2=0,PTGSE1=0,PTGSE0=0 */
  PTGSE &= (unsigned char)~0x7F;
  /* PTADS: PTADS7=0,PTADS6=0,PTADS5=0,PTADS4=0,PTADS3=0,PTADS2=0,PTADS1=0,PTADS0=0 */
  PTADS = 0x00;
  /* PTBDS: PTBDS7=0,PTBDS6=0,PTBDS5=0,PTBDS4=0,PTBDS3=0,PTBDS2=0,PTBDS1=0,PTBDS0=0 */

  /* PTCDS: PTCDS6=0,PTCDS5=0,PTCDS4=0,PTCDS3=0,PTCDS2=0,PTCDS1=0,PTCDS0=0 */
  PTCDS = 0x00;
  /* PTDDS: PTDDS7=0,PTDDS6=0,PTDDS5=0,PTDDS4=0,PTDDS3=0,PTDDS2=0,PTDDS1=0,PTDDS0=0 */
  PTDDS = 0x00;
  /* PTEDS: PTEDS7=0,PTEDS6=0,PTEDS5=0,PTEDS4=0,PTEDS3=0,PTEDS2=0,PTEDS1=0,PTEDS0=0 */
  PTEDS = 0x00;
  /* PTFDS: PTFDS7=0,PTFDS6=0,PTFDS5=0,PTFDS4=0,PTFDS3=0,PTFDS2=0,PTFDS1=0,PTFDS0=0 */
  PTFDS = 0x00;
  /* PTGDS: PTGDS6=0,PTGDS5=0,PTGDS4=0,PTGDS3=0,PTGDS2=0,PTGDS1=0,PTGDS0=0 */
  PTGDS = 0x00;
  /* ### Init_GPIO init code */
  /* PTADD: PTADD7=1,PTADD6=1,PTADD5=1,PTADD4=1,PTADD3=1,PTADD2=1,PTADD1=1,PTADD0=1 */
  PTADD = 0xFF;
  /* ### Init_GPIO init code */
  /* PTBDD: PTBDD2=1,PTBDD1=1,PTBDD0=1 */
  PTBDD |= (unsigned char)0x07;
  /* ### Init_GPIO init code */
  /* PTDDD: PTDDD4=0,PTDDD3=0,PTDDD2=0,PTDDD1=0,PTDDD0=0 */
  PTDDD &= (unsigned char)~0x1F;
  /* ### Init_GPIO init code */
  /* PTFDD: PTFDD4=0,PTFDD3=0,PTFDD2=0,PTFDD1=0,PTFDD0=0 */
  PTFDD &= (unsigned char)~0x1F;
  /* ### */
  asm CLI; /* Enable interrupts */
} /*MCU_init*/

/*
** ===================================================================
**     Interrupt handler : isrVicg
**
**     Description :
**         User interrupt service routine. 
**     Parameters  : None
**     Returns     : Nothing
** ===================================================================
*/

